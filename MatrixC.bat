@echo off
"C:\Program Files\Java\jdk1.7.0_79\bin\java.exe" -jar MatrixCompiler.jar %~f1
if exist .\Main.java ( 
"C:\Program Files\Java\jdk1.7.0_79\bin\javac.exe" -cp lib\antlr-3.5.2-complete.jar;lib\Jama-1.0.3.jar;MatrixCompiler.jar Main.java
"C:\Program Files\Java\jdk1.7.0_79\bin\java.exe" -cp bin;lib\antlr-3.5.2-complete.jar;lib\Jama-1.0.3.jar;MatrixCompiler.jar Main
) else ( echo System message : source java file doesn't exist. Exiting compilation)