package block;

import java.util.HashMap;
import java.util.Map;

public class Block {
	private Map<String, Integer> vars;

	public Block() {
		vars = new HashMap<String, Integer>();
	}

	public boolean containsVar(String varName) {
		return vars.containsKey(varName);
	}

	public void addVar(String varName, Integer line) {
		vars.put(varName, line);
	}

	public int getVarLine(String varName) {
		return vars.get(varName);
	}
}
