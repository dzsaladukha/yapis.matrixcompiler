package block;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Blocks {
	private List<Block> blocks;

	public Blocks() {
		blocks = new ArrayList<>();
	}

	public void addVar(String varName, int line) {
		try {
			getLastBlock().addVar(varName, line);
		} catch (NullPointerException e) {
			throw new RuntimeException("Blocks are not exist.");
		}
	}

	public boolean containsVar(String varName) {
		final Block block = getLastBlock();
		return (block == null) ? false : block.containsVar(varName);
	}

	public boolean containsVarInAllBlocks(String varName) {
		boolean contain = false;
		Iterator<Block> itr = blocks.iterator();
		while (itr.hasNext()) {
			final Block block = itr.next();
			if (block.containsVar(varName)) {
				contain = true;
				break;
			}
		}
		return contain;
	}

	public int getVarLine(String varName) {
		int line = -1;
		Iterator<Block> itr = blocks.iterator();
		while (itr.hasNext()) {
			final Block block = itr.next();
			if (block.containsVar(varName)) {
				line = block.getVarLine(varName);
				break;
			}
		}
		if (line == -1)
			throw new RuntimeException("Such variable is not exist: " + varName);
		return line;
	}

	private Block getLastBlock() {
		return blocks.isEmpty() ? null : blocks.get(blocks.size() - 1);
	}

	public void addBlock() {
		blocks.add(new Block());
	}

	public void removeBlock() {
		blocks.remove(blocks.size() - 1);
	}
}
