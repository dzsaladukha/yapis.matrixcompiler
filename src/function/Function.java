package function;

import java.util.List;

public class Function {
	private String name;
	private List<String> params;

	public Function(String name, List<String> params) {
		this.setName(name);
		this.setParams(params);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getParams() {
		return params;
	}

	public void setParams(List<String> param) {
		this.params = param;
	}
}
