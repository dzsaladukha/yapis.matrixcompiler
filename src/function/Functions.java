package function;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import error.Errors;
import function.Function;

public class Functions {
	private List<Function> functions;

	public Functions() {
		functions = new ArrayList<Function>();
	}

	public boolean containsFunc(String name) {
		boolean result = false;
		Iterator<Function> itr = functions.iterator();
		while (itr.hasNext()) {
			Function function = itr.next();
			if (function.getName().equals(name)) {
				result = true;
				break;
			}
		}
		return result;
	}

	public void addFunc(String name, List<String> param) {
		functions.add(new Function(name, param));
	}

	public void checkParamsType(String name, List<String> param, int line,
			Errors errors) {
		Iterator<Function> itr = functions.iterator();
		while (itr.hasNext()) {
			Function function = itr.next();
			if (function.getName().equals(name)) {
				if (!function.getParams().equals(param)) {
					errors.addError(
							"The function " + name
									+ getParam(function.getParams())
									+ " is not applicable for the arguments "
									+ getParam(param), line);
				}
			}
		}
	}

	private String getParam(List<String> param) {
		String result = "[";
		Iterator<String> itr = param.iterator();
		while (itr.hasNext()) {
			result += itr.next() + " ";
		}
		result += "]";
		return result;
	}
}
