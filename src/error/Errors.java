package error;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Errors {
	private List<Error> errors;

	public Errors() {
		errors = new ArrayList<Error>();
	}

	public void printErrors() {
		Iterator<Error> itr = errors.iterator();
		while (itr.hasNext()) {
			final Error error = itr.next();
			String description = error.getDescription();
			int line = error.getLine();
			System.err.println(description + " Line " + line);
		}
	}

	public void addError(String description, int line) {
		errors.add(new Error(description, line));
	}
}
