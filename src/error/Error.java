package error;

public class Error {
	private String description;
	private int line;

	Error(String description, int line) {

		this.description = description;
		this.line = line;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}
}
