package main;

import grammar.MatrixLexer;
import grammar.MatrixParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;

public class EntryPoint {

	public static void main(String[] args) throws IOException,
			RecognitionException {
		if (args.length == 0)
			return;
		FileReader gf = new FileReader("src/Matrix.stg");
		StringTemplateGroup stg = new StringTemplateGroup(gf);
		gf.close();
		File file = new File(args[0]);
		InputStream fis = new FileInputStream(file);
		try {
			ANTLRInputStream input = new ANTLRInputStream(fis);
			MatrixLexer lexer = new MatrixLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			MatrixParser parser = new MatrixParser(tokens);
			parser.setTemplateLib(stg);
			MatrixParser.program_return r = parser.program();
			StringTemplate output = (StringTemplate) r.getTemplate();
			System.out.println(output.toString());
			PrintWriter printer = new PrintWriter("src/main/Main.java", "utf-8");
			printer.print(output.toString());
			printer.close();
			Runtime.getRuntime().exec("cmd /c start run.bat");
		} catch (Exception e) {
			System.out.println(e.toString());
		}

	}
}