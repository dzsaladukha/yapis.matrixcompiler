package types;

public class Matrix extends Variable {
	public Matrix(double[] arr, int rows) {
		value = new Jama.Matrix(arr, rows);
		type = "Matrix";
	}

	public Matrix(double[][] mat) {
		this.value = new Jama.Matrix(mat);
		type = "Matrix";
	}

	public Matrix(Jama.Matrix value) {
		this.value = value;
		type = "Matrix";
	}

	@Override
	public void print() {
		((Jama.Matrix) value).print(((Jama.Matrix) value).getColumnDimension(),
				((Jama.Matrix) value).getRowDimension());
	}

	@Override
	public Variable plus(Variable var) {
		String type = var.getType();
		if (type == "Vector" || type == "Matrix") {
			Jama.Matrix mat = ((Jama.Matrix) value).plus((Jama.Matrix) var
					.getValue());
			return new types.Matrix(mat);
		}
		return new Variable();
	}

	@Override
	public Variable minus(Variable var) {
		String type = var.getType();
		if (type == "Vector" || type == "Matrix") {
			Jama.Matrix mat = ((Jama.Matrix) value).minus((Jama.Matrix) var
					.getValue());
			return new types.Matrix(mat);
		}
		return new Variable();
	}

	@Override
	public Variable mult(Variable var) {
		String type = var.getType();
		if (type == "Vector" || type == "Matrix") {
			Jama.Matrix mat = ((Jama.Matrix) value).times((Jama.Matrix) var
					.getValue());
			return new types.Matrix(mat);
		} else if (type == "Number") {
			Jama.Matrix mat = ((Jama.Matrix) value).times((double) var
					.getValue());
			return new types.Matrix(mat);
		}
		return new Variable();
	}

	@Override
	public Variable div(Variable var) {
		String type = var.getType();
		if (type == "Number") {
			Jama.Matrix mat = ((Jama.Matrix) value).times(1 / (double) var
					.getValue());
			return new types.Matrix(mat);
		}
		return new Variable();
	}

	@Override
	public boolean equal(Variable var) {
		if (var.getType() == "Matrix") {
			double[][] first = ((Jama.Matrix) value).getArray();
			double[][] second = ((Jama.Matrix) var.getValue()).getArray();
			int row = getRow();
			int column = getColumn();
			for (int i = 0; i < column; i++) {
				for (int j = 0; j < row; j++) {
					if (first[i][j] != second[i][j])
						return false;
				}
			}
			return true;
		}
		return false;
	}

	public int getColumn() {
		return ((Jama.Matrix) value).getRowDimension();
	}

	public int getRow() {
		return ((Jama.Matrix) value).getColumnDimension();
	}

	public void set(int i, int j, double value) {
		((Jama.Matrix) this.value).set(i, j, value);
	}

	public double get(int i, int j) {
		return ((Jama.Matrix) this.value).get(i, j);
	}
}
