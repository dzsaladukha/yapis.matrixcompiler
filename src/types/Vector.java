package types;

import Jama.Matrix;

public class Vector extends Variable {
	public Vector(double[] arg0) {
		value = new Jama.Matrix(arg0, arg0.length);
		type = "Vector";
	}

	public Vector(Jama.Matrix value) {
		this.value = value;
		type = "Vector";
	}

	public double get(int index) {
		return ((Matrix) value).get(index, 0);
	}

	public int getSize() {
		return ((Matrix) value).getRowDimension();
	}

	public void set(int index, double d) {
		((Matrix) this.value).set(index, 0, d);
	}

	@Override
	public void print() {
		((Matrix) value).print(((Matrix) value).getColumnDimension(),
				((Matrix) value).getRowDimension());
	}

	@Override
	public Variable plus(Variable var) {
		String type = var.getType();
		if (type == "Vector" || type == "Matrix") {
			Jama.Matrix mat = ((Matrix) value).plus((Matrix) var.getValue());
			if (mat.getColumnDimension() == 1) {
				return new Vector(mat);
			} else {
				return new types.Matrix(mat);
			}
		}
		return new Variable();
	}

	@Override
	public Variable minus(Variable var) {
		String type = var.getType();
		if (type == "Vector" || type == "Matrix") {
			Jama.Matrix mat = ((Matrix) value).minus((Matrix) var.getValue());
			if (mat.getColumnDimension() == 1) {
				return new Vector(mat);
			} else {
				return new types.Matrix(mat);
			}
		}
		return new Variable();
	}

	@Override
	public Variable mult(Variable var) {
		String type = var.getType();
		if (type == "Vector" || type == "Matrix") {
			Jama.Matrix mat = ((Matrix) value).times((Matrix) var.getValue());
			if (mat.getColumnDimension() == 1) {
				return new Vector(mat);
			} else {
				return new types.Matrix(mat);
			}
		} else if (type == "Number") {
			Jama.Matrix mat = ((Matrix) value).times((double) var.getValue());
			return new Vector(mat);
		}
		return new Variable();
	}

	@Override
	public Variable div(Variable var) {
		String type = var.getType();
		if (type == "Number") {
			Jama.Matrix mat = ((Matrix) value).times(1 / (double) var
					.getValue());
			return new Vector(mat);
		}
		return new Variable();
	}

	@Override
	public boolean equal(Variable var) {
		if (var.getType() == "Vector") {
			double[][] first = ((Jama.Matrix) value).getArray();
			double[][] second = ((Jama.Matrix) var.getValue()).getArray();
			int row = getSize();
			for (int i = 0; i < row; i++) {
				if (first[i][0] != second[i][0])
					return false;
			}
			return true;
		}
		return false;
	}
}
