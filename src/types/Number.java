package types;

public class Number extends Variable{	
	public Number(double value){
		this.value = value;
		type = "Number";
	}

	@Override
	public void print() {
		System.out.println((Double) value);
	}
	
	@Override
	public Variable plus(Variable var) {
		String type = var.getType();
		if(type == "Number")
			return new Number((double)value + (double)var.getValue()); 
		return new Variable();
	}
	
	@Override
	public Variable minus(Variable var) {
		String type = var.getType();
		if(type == "Number")
			return new Number((double)value - (double)var.getValue()); 
		return new Variable();
	}

	@Override
	public Variable mult(Variable var) {
		String type = var.getType();
		if(type == "Number")
			return new Number((double)value * (double)var.getValue()); 
		if(type == "Matrix" || type == "Vector")
			return var.mult(this);
		return new Variable();
	}
	
	@Override
	public Variable div(Variable var) {
		String type = var.getType();
		if(type == "Number")
			return new Number((double)value / (double)var.getValue()); 
		return new Variable();
	}
}
