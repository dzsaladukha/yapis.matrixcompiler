package types;

public class Variable {
	protected String type;
	protected Object value;

	public Variable() {
		type = "void";
		value = "undefined";
	}
	
	public String getType() {
		return type;
	}

	public void print() {
		System.out.println((String) value);
	};
	
	public Object getValue() {
		return value;
	}
	
	public Variable plus(Variable var) {
		return this;
	}
	
	public Variable minus(Variable var) {
		return this;
	}
	
	public Variable mult(Variable var) {
		return this;
	}
	
	public Variable div(Variable var) {
		return this;
	}

	public boolean equal(Variable second) {
		if(second.getType() == "void")
			return true;
		return false;
	}
}
