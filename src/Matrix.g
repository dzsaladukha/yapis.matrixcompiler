grammar Matrix;

options {
  language = Java;
  output   = template;
}

tokens {
  DEFINE      = 'def';
  RETURN      = 'return';
  MAIN        = 'Main';
  BOR         = '|';
  L_RBR       = '(';
  R_RBR       = ')';
  L_CBR       = '{';
  R_CBR       = '}';
  L_BR        = '[';
  R_BR        = ']';
  COLON       = ':';
  COMMA       = ',';
  QUOTE       = '"';
  SEMI        = ';';
  ASSIGN      = '=';
  AND         = 'and';
  FOR         = 'for';
  NOT         = 'not';
  OR          = 'or';
  IF          = 'if';
  ELSE        = 'else';
  WHILE       = 'while';
  REPEAT      = 'repeat';
  UNTIL       = 'until';
  PLUS        = '+';
  MINUS       = '-';
  MULT        = '*';
  DIV         = '/';
  FUN_PRINT   = 'print';
  END         = 'end';
  DISJ        = '||';
  CONJ        = '&&';
  POINT       = '.';
  SIZE        = 'size';
  ROW         = 'row';
  COLUMN      = 'column';
  PUTS        = 'puts ';
  VAR         = 'var';
  UNDEFINED   = 'undefined';
}

scope slist {
	List locals;
	List stats;
}

@header {
package grammar;
    import block.*;
    import error.*;
    import function.*;
}

@lexer::header {
package grammar;
    import block.*;
    import error.*; 
    import function.*;
}

@members {
	Blocks blocks = new Blocks();
	Errors errors = new Errors();
	Functions functions = new Functions();

	// for string templates
	public String toString(String str){
	  return "\"" + str + "\"";
  }
}

program:
  (funcs += functionDeclaration)*
  MAIN COLON
    {
		 blocks.addBlock();
		}
		  (stats += statement)*
		{
		 blocks.removeBlock();
		}
  END
  {
   errors.printErrors();
  }
  -> program(funcs = {$funcs}, stats = {$stats})
  ;

functionDeclaration
scope {
  List<String> params;
}
@init {
  $functionDeclaration::params = new ArrayList<String>();
}:
  DEFINE name=ID (L_RBR (p=ID{$functionDeclaration::params.add($p.text);}
                  (COMMA p=ID{$functionDeclaration::params.add($p.text);})*)? R_RBR)? COLON
    {
     blocks.addBlock();
     for(String id: $functionDeclaration::params) {
        blocks.addVar(id,$L_RBR.line);
     }
     functions.addFunc($name.text,$functionDeclaration::params);
    }
		(stats += statement)*
		(RETURN e=expression)?
		{
		 blocks.removeBlock();
		 if($functionDeclaration::params.size() > 0) {
        String firstParam = $functionDeclaration::params.get(0);
        $functionDeclaration::params.set(0, "Variable " + firstParam);
     }
		 if($e.st != null) {
		    $st = %functionDeclaration(name={$name.text},params={$functionDeclaration::params},returned={$expression.st},stats={$stats});
		 } else {
		    $st = %voidFunctionDeclaration(name={$name.text},params={$functionDeclaration::params},stats={$stats});
		 }
		}
  END
  ;

functionCall:
  name=ID L_RBR (p+=expression
          (COMMA p+=expression)*)? R_RBR
  {
   if(!functions.containsFunc($name.text)){
      errors.addError("Undefined method " + $name.text + ".",$name.line);
   }
  }
  -> functionCall(name = {$name.text}, params = {$p})
  ;

expression:
  opd+=subexpr (op=(PLUS|MINUS|MULT|DIV) opd+=subexpr)?
  {
   if($op != null) {
      switch($op.text) {
        case "+":
          $st = %plus_template(operands={$opd});
          break;
        case "-":
          $st = %minus_template(operands={$opd});
          break;
        case "*":
          $st = %mult_template(operands={$opd});
          break;
        case "/":
          $st = %div_template(operands={$opd});
          break;
      }
   } else $st = %atom_template(operands={$opd});
  }
  ;

subexpr:
  functionCall
  -> {$functionCall.st;}
  |
  L_RBR expression R_RBR
  -> {$expression.st;}
  |
  ID
  {
   if(!blocks.containsVarInAllBlocks($ID.text)) {
      errors.addError("Not defined variable " + $ID.text + ".", $ID.line);
   }
  }
  -> {new StringTemplate($ID.text);}
  |
  number
  -> initNumber(val = {$number.st})
  |
  initializationVector
  -> {$initializationVector.st}
  |
  initializationMatrix
  -> {$initializationMatrix.st}
  |
  getter
  -> {$getter.st}
  |
  sizeRowColumn
  -> {$sizeRowColumn.st}
  |
  UNDEFINED
  -> undefined()
  ;

sizeRowColumn:
  ID POINT what=(SIZE|ROW|COLUMN)
  {
   if(!blocks.containsVarInAllBlocks($ID.text)) {
      errors.addError("Undefined variable " + $ID.text + ".",$ID.line);
   } else {
			switch($what.text) {
			 case "size":
			   $st = %size(name={$ID.text});
			   break;
			 case "row":
			   $st = %row(name={$ID.text});
			   break;
			 case "column":
			   $st = %column(name={$ID.text});
			   break;
			}
   }
  }
  ;

getter:
  ID L_BR f=expression R_BR (L_BR s=expression R_BR)?
  {
   if(!blocks.containsVarInAllBlocks($ID.text)) {
      errors.addError("Undefined variable " + $ID.text + ".",$ID.line);
   } else if ($s.st != null) {
      $st = %matrixGetter(var={$ID.text},frst={$f.st},second={$s.st});
   } else {
      $st = %vectorGetter(var={$ID.text},frst={$f.st});
   }
  }
  ;

statement:
    var_define   -> statement(st = {$var_define.st})
  | putStat      -> statement(st = {$putStat.st})
  | functionCall -> statement(st = {$functionCall.st})
  | whileStat    -> {$whileStat.st}
  | untilStat    -> {$untilStat.st}
  | forStat      -> {$forStat.st}
  | ifElseStat   -> {$ifElseStat.st}
  | setter       -> statement(st = {$setter.st})
  ;

setter:
  ID L_BR f=expression R_BR (L_BR s=expression R_BR)? ASSIGN val=expression
  {
   if(!blocks.containsVarInAllBlocks($ID.text)) {
      errors.addError("Undefined variable " + $ID.text + ".",$ID.line);
   } else if ($s.st != null) {
      $st = %matrixSetter(var={$ID.text},frst={$f.st},second={$s.st},value={$val.st});
   } else {
      $st = %vectorSetter(var={$ID.text},frst={$f.st},value={$val.st});
   }
  }
  ;

ifElseStat:
  IF L_RBR log_expr R_RBR COLON
      // TODO: add blocks
      (stats+=statement)*
  (ELSE
      (else_stats+=statement)*)?
  END
  -> ifElseStat(cond={$log_expr.st},stats={$stats},else_stats={$else_stats})
  ;

forStat:
  FOR L_RBR (a=var_define)? SEMI (log_expr)? SEMI (b=var_define)? R_RBR COLON
      // TODO: add block
      (stats+=statement)* 
  END
  -> forStat(init={$a.st}, 
									cond={$log_expr.st}, 
									assign={$b.st},
									stats={$stats});

whileStat:
  WHILE L_RBR log_expr R_RBR COLON
      // TODO: add block
      (stats+=statement)*
  END
  -> whileStat(cond={$log_expr.st},stats={$stats})
  ;

untilStat:
  REPEAT COLON
      // TODO: add block
      (stats+=statement)*
  UNTIL L_RBR log_expr R_RBR
  -> untilStat(cond={$log_expr.st},stats={$stats})
  ;

log_expr:
  sublog_expr (op=(DISJ|CONJ) l=log_expr)?
  {
   if($op != null) {
      if($op.text.equals("||")) {
        $st = %disjunction(frst={$sublog_expr.st},second={$l.st});
      } else {
        $st = %conjunction(frst={$sublog_expr.st},second={$l.st});
      }
   } else {
      $st = $sublog_expr.st;
   }
  }
  ;

sublog_expr :
  first=expression LOG_SIGN second=expression 
   {
    switch($LOG_SIGN.text) {
      case "==":
        $st = %equal(frst={$first.st},second={$second.st});
        break;
      case "!=":
        $st = %notEqual(frst={$first.st},second={$second.st});
        break;
      case ">=":
        $st = %greaterEqual(frst={$first.st},second={$second.st});
        break;
      case "<=":
        $st = %lessEqual(frst={$first.st},second={$second.st});
        break;
      case ">":
        $st = %greater(frst={$first.st},second={$second.st});
        break;
      case "<":
        $st = %less(frst={$first.st},second={$second.st});
        break;
    }
  }
  ;

var_define:
  VAR ID
  {
   if(blocks.containsVar($ID.text)) {
      errors.addError("Duplicate local variable " + $ID.text, $ID.line);
   } else {
      blocks.addVar($ID.text, $ID.line);
   }
  }
  -> var_default_define(name = {$ID.text})
  |
  VAR ID ASSIGN var_value=expression
  {
   if(blocks.containsVar($ID.text)) {
      errors.addError("Duplicate local variable " + $ID.text + ".", $ID.line);
   } else {
      blocks.addVar($ID.text, $ID.line);
   } 
  }
  -> var_define(name = {$ID.text}, value = {$var_value.st})
  |
  ID ASSIGN var_value=expression
  {
   if(blocks.containsVar($ID.text)) {
      $st = %var_assign(name={$ID.text},value={$var_value.st});
   } else {
			blocks.addVar($ID.text, $ID.line);
      $st = %var_define(name={$ID.text},value={$var_value.st});
   }
  }
  ;

number:
  MINUS? NUMBER
  -> {new StringTemplate(($MINUS != null ? $MINUS.text : "") + $NUMBER.text);}
  ;

initializationVector:
  L_BR vals += number(COMMA vals += number)* R_BR
  -> initVector(vals = {$vals})
  ;

initializationMatrix
scope {
  int matrixRow;
  int matrixColumn;
}
@init {
  $initializationMatrix::matrixRow = 1;
}:
  L_CBR vals += number
    ((COMMA vals += number)
      | BOR{$initializationMatrix::matrixRow++;} vals += number)*R_CBR
  {
   $initializationMatrix::matrixColumn = $vals.size() / $initializationMatrix::matrixRow;
   if($vals.size() != $initializationMatrix::matrixRow * $initializationMatrix::matrixColumn){
       errors.addError("Initialization matrix error", $L_CBR.line);
   }
  }
  -> initMatrix(vals = {$vals}, rows = {$initializationMatrix::matrixRow})
  ;

putStat:
  PUTS STRING
  -> printString(s = {$STRING.text})
  |
  PUTS expression
  -> printVar(var = {$expression.st})
  ;

ID: 
  ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

STRING:
  '"'( options {greedy=false;} : . )*'"'
  ;
 
NUMBER: 
  ('0'..'9')+('.'('0'..'9')*)?;

LOG_SIGN:
  '<'|'>'|'=='|'!='|'<='|'>=';
  
COMMENT:
  '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
  | '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;};
  
WS: 
  (' ' | '\t' | '\r' | '\n')+ {$channel=HIDDEN;};
