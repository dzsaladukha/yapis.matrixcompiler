package utils;

import types.Variable;
import types.Number;
import types.Vector;
import types.Matrix;

public class VarBuilder {	
	public static Variable createNumber(double value) {
		return new Number(value);
	}
	
	public static Variable createVector(double[] arr) {
		return new Vector(arr);
	}
	
	public static Variable createMatrix(double[] arr, int rows) {
		int columns = arr.length / rows;
		double [][] mat = new double[rows][columns];
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < columns; j++){
				mat[i][j] = arr[i * columns + j];
			}
		}
		return new Matrix(mat);
	}
}
