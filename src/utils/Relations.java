package utils;

import types.Variable;

public class Relations {
	public static boolean equal(Variable first, Variable second) {
		if (first.getType().equals(second.getType())) {
			if (first.getType() == "Number") {
				return (double) first.getValue() == (double) second.getValue();
			} else {
				if (first.getType() != "void")
					return first.equal(second);
				else
					return true;
			}
		} else if ((first.getType() == "Vector" && second.getType() == "Matrix")
				|| (second.getType() == "Vector" && first.getType() == "Matrix")) {
			return first.equal(second);
		}
		return false;
	}

	public static boolean notEqual(Variable first, Variable second) {
		if (first.getType() == "void" || second.getType() == "void")
			return false;
		return !equal(first, second);
	}

	public static boolean greater/* > */(Variable first, Variable second) {
		if (first.getType() == "Number" || second.getType() == "Number")
			return (double) first.getValue() > (double) second.getValue();
		return false;
	}

	public static boolean less/* < */(Variable first, Variable second) {
		if (first.getType() == "Number" || second.getType() == "Number")
			return (double) first.getValue() < (double) second.getValue();
		return false;
	}

	public static boolean greaterEqual(Variable first, Variable second) {
		if (first.getType() == "Number" || second.getType() == "Number")
			return !less(first, second);
		return false;
	}

	public static boolean lessEqual(Variable first, Variable second) {
		if (first.getType() == "Number" && second.getType() == "Number")
			return !greater(first, second);
		return false;
	}

	public static boolean disjunction(boolean first, boolean second) {
		return first || second;
	}

	public static boolean conjunction(boolean first, boolean second) {
		return first && second;
	}
}
