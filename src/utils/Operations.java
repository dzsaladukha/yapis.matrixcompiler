package utils;

import types.Variable;

public class Operations {
	public static void hi() {
		System.out.println("Hello world! nyaaa");
	}

	public static void println(String str) {
		System.out.println(str);
	}

	public static Variable plus(Variable first, Variable second) {
		return first.plus(second);
	}

	public static Variable minus(Variable first, Variable second) {
		return first.minus(second);
	}

	public static Variable mult(Variable first, Variable second) {
		return first.mult(second);
	}

	public static Variable div(Variable first, Variable second) {
		return first.div(second);
	}

	public static void setter(Variable var, Variable first, Variable second,
			Variable value) {
		if (var.getType() == "Matrix" && first.getType() == "Number"
				&& second.getType() == "Number" && value.getType() == "Number") {
			((types.Matrix) var).set(((Double) first.getValue()).intValue(),
					((Double) second.getValue()).intValue(),
					(double) value.getValue());
		} else {
			throw new RuntimeException("Bad setter for matrix");
		}
	}

	public static Variable getter(Variable var, Variable first, Variable second) {
		if (var.getType() == "Matrix" && first.getType() == "Number"
				&& second.getType() == "Number") {
			return VarBuilder.createNumber(((types.Matrix) var).get(
					((Double) first.getValue()).intValue(),
					((Double) second.getValue()).intValue()));
		} else {
			return new Variable();
		}
	}

	public static void setter(Variable var, Variable first, Variable value) {
		if (var.getType() == "Vector" && first.getType() == "Number"
				&& value.getType() == "Number") {
			((types.Vector) var).set(((Double) first.getValue()).intValue(),
					(double) value.getValue());
		} else {
			throw new RuntimeException("Bad setter for vector");
		}
	}

	public static Variable getter(Variable var, Variable first) {
		if (var.getType() == "Vector" && first.getType() == "Number") {
			return VarBuilder.createNumber(((types.Vector) var)
					.get(((Double) first.getValue()).intValue()));
		} else {
			return new Variable();
		}
	}
	
	public static Variable getSize(Variable var) {
		if(var.getType() == "Vector")
			return VarBuilder.createNumber(((types.Vector) var).getSize());
		return new Variable();
	}
	
	public static Variable getRow(Variable var) {
		if(var.getType() == "Matrix")
			return VarBuilder.createNumber(((types.Matrix) var).getRow());
		return new Variable();
	}
	
	public static Variable getColumn(Variable var) {
		if(var.getType() == "Matrix")
			return VarBuilder.createNumber(((types.Matrix) var).getColumn());
		return new Variable();
	}
}
