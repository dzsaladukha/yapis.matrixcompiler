// $ANTLR 3.5.2 G:\\wsi2\\MatrixCompiler\\src\\Matrix.g 2016-09-06 01:13:01

package grammar;
    import block.*;
    import error.*; 
    import function.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class MatrixLexer extends Lexer {
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ASSIGN=5;
	public static final int BOR=6;
	public static final int COLON=7;
	public static final int COLUMN=8;
	public static final int COMMA=9;
	public static final int COMMENT=10;
	public static final int CONJ=11;
	public static final int DEFINE=12;
	public static final int DISJ=13;
	public static final int DIV=14;
	public static final int ELSE=15;
	public static final int END=16;
	public static final int FOR=17;
	public static final int FUN_PRINT=18;
	public static final int ID=19;
	public static final int IF=20;
	public static final int LOG_SIGN=21;
	public static final int L_BR=22;
	public static final int L_CBR=23;
	public static final int L_RBR=24;
	public static final int MAIN=25;
	public static final int MINUS=26;
	public static final int MULT=27;
	public static final int NOT=28;
	public static final int NUMBER=29;
	public static final int OR=30;
	public static final int PLUS=31;
	public static final int POINT=32;
	public static final int PUTS=33;
	public static final int QUOTE=34;
	public static final int REPEAT=35;
	public static final int RETURN=36;
	public static final int ROW=37;
	public static final int R_BR=38;
	public static final int R_CBR=39;
	public static final int R_RBR=40;
	public static final int SEMI=41;
	public static final int SIZE=42;
	public static final int STRING=43;
	public static final int UNDEFINED=44;
	public static final int UNTIL=45;
	public static final int VAR=46;
	public static final int WHILE=47;
	public static final int WS=48;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public MatrixLexer() {} 
	public MatrixLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public MatrixLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "G:\\wsi2\\MatrixCompiler\\src\\Matrix.g"; }

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:14:5: ( 'and' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:14:7: 'and'
			{
			match("and"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "ASSIGN"
	public final void mASSIGN() throws RecognitionException {
		try {
			int _type = ASSIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:15:8: ( '=' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:15:10: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASSIGN"

	// $ANTLR start "BOR"
	public final void mBOR() throws RecognitionException {
		try {
			int _type = BOR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:16:5: ( '|' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:16:7: '|'
			{
			match('|'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOR"

	// $ANTLR start "COLON"
	public final void mCOLON() throws RecognitionException {
		try {
			int _type = COLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:17:7: ( ':' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:17:9: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLON"

	// $ANTLR start "COLUMN"
	public final void mCOLUMN() throws RecognitionException {
		try {
			int _type = COLUMN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:18:8: ( 'column' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:18:10: 'column'
			{
			match("column"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLUMN"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:19:7: ( ',' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:19:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "CONJ"
	public final void mCONJ() throws RecognitionException {
		try {
			int _type = CONJ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:20:6: ( '&&' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:20:8: '&&'
			{
			match("&&"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CONJ"

	// $ANTLR start "DEFINE"
	public final void mDEFINE() throws RecognitionException {
		try {
			int _type = DEFINE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:21:8: ( 'def' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:21:10: 'def'
			{
			match("def"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DEFINE"

	// $ANTLR start "DISJ"
	public final void mDISJ() throws RecognitionException {
		try {
			int _type = DISJ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:22:6: ( '||' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:22:8: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DISJ"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:23:5: ( '/' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:23:7: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:24:6: ( 'else' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:24:8: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "END"
	public final void mEND() throws RecognitionException {
		try {
			int _type = END;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:25:5: ( 'end' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:25:7: 'end'
			{
			match("end"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "END"

	// $ANTLR start "FOR"
	public final void mFOR() throws RecognitionException {
		try {
			int _type = FOR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:26:5: ( 'for' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:26:7: 'for'
			{
			match("for"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FOR"

	// $ANTLR start "FUN_PRINT"
	public final void mFUN_PRINT() throws RecognitionException {
		try {
			int _type = FUN_PRINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:27:11: ( 'print' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:27:13: 'print'
			{
			match("print"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FUN_PRINT"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:28:4: ( 'if' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:28:6: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "L_BR"
	public final void mL_BR() throws RecognitionException {
		try {
			int _type = L_BR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:29:6: ( '[' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:29:8: '['
			{
			match('['); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "L_BR"

	// $ANTLR start "L_CBR"
	public final void mL_CBR() throws RecognitionException {
		try {
			int _type = L_CBR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:30:7: ( '{' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:30:9: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "L_CBR"

	// $ANTLR start "L_RBR"
	public final void mL_RBR() throws RecognitionException {
		try {
			int _type = L_RBR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:31:7: ( '(' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:31:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "L_RBR"

	// $ANTLR start "MAIN"
	public final void mMAIN() throws RecognitionException {
		try {
			int _type = MAIN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:32:6: ( 'Main' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:32:8: 'Main'
			{
			match("Main"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MAIN"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:33:7: ( '-' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:33:9: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MULT"
	public final void mMULT() throws RecognitionException {
		try {
			int _type = MULT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:34:6: ( '*' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:34:8: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULT"

	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			int _type = NOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:35:5: ( 'not' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:35:7: 'not'
			{
			match("not"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:36:4: ( 'or' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:36:6: 'or'
			{
			match("or"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:37:6: ( '+' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:37:8: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "POINT"
	public final void mPOINT() throws RecognitionException {
		try {
			int _type = POINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:38:7: ( '.' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:38:9: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "POINT"

	// $ANTLR start "PUTS"
	public final void mPUTS() throws RecognitionException {
		try {
			int _type = PUTS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:39:6: ( 'puts ' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:39:8: 'puts '
			{
			match("puts "); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PUTS"

	// $ANTLR start "QUOTE"
	public final void mQUOTE() throws RecognitionException {
		try {
			int _type = QUOTE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:40:7: ( '\"' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:40:9: '\"'
			{
			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "QUOTE"

	// $ANTLR start "REPEAT"
	public final void mREPEAT() throws RecognitionException {
		try {
			int _type = REPEAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:41:8: ( 'repeat' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:41:10: 'repeat'
			{
			match("repeat"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "REPEAT"

	// $ANTLR start "RETURN"
	public final void mRETURN() throws RecognitionException {
		try {
			int _type = RETURN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:42:8: ( 'return' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:42:10: 'return'
			{
			match("return"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RETURN"

	// $ANTLR start "ROW"
	public final void mROW() throws RecognitionException {
		try {
			int _type = ROW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:43:5: ( 'row' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:43:7: 'row'
			{
			match("row"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ROW"

	// $ANTLR start "R_BR"
	public final void mR_BR() throws RecognitionException {
		try {
			int _type = R_BR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:44:6: ( ']' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:44:8: ']'
			{
			match(']'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "R_BR"

	// $ANTLR start "R_CBR"
	public final void mR_CBR() throws RecognitionException {
		try {
			int _type = R_CBR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:45:7: ( '}' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:45:9: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "R_CBR"

	// $ANTLR start "R_RBR"
	public final void mR_RBR() throws RecognitionException {
		try {
			int _type = R_RBR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:46:7: ( ')' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:46:9: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "R_RBR"

	// $ANTLR start "SEMI"
	public final void mSEMI() throws RecognitionException {
		try {
			int _type = SEMI;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:47:6: ( ';' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:47:8: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMI"

	// $ANTLR start "SIZE"
	public final void mSIZE() throws RecognitionException {
		try {
			int _type = SIZE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:48:6: ( 'size' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:48:8: 'size'
			{
			match("size"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SIZE"

	// $ANTLR start "UNDEFINED"
	public final void mUNDEFINED() throws RecognitionException {
		try {
			int _type = UNDEFINED;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:49:11: ( 'undefined' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:49:13: 'undefined'
			{
			match("undefined"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "UNDEFINED"

	// $ANTLR start "UNTIL"
	public final void mUNTIL() throws RecognitionException {
		try {
			int _type = UNTIL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:50:7: ( 'until' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:50:9: 'until'
			{
			match("until"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "UNTIL"

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:51:5: ( 'var' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:51:7: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "WHILE"
	public final void mWHILE() throws RecognitionException {
		try {
			int _type = WHILE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:52:7: ( 'while' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:52:9: 'while'
			{
			match("while"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHILE"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:402:3: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:403:3: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:403:27: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:405:7: ( '\"' ( options {greedy=false; } : . )* '\"' )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:406:3: '\"' ( options {greedy=false; } : . )* '\"'
			{
			match('\"'); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:406:6: ( options {greedy=false; } : . )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( (LA2_0=='\"') ) {
					alt2=2;
				}
				else if ( ((LA2_0 >= '\u0000' && LA2_0 <= '!')||(LA2_0 >= '#' && LA2_0 <= '\uFFFF')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:406:34: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop2;
				}
			}

			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING"

	// $ANTLR start "NUMBER"
	public final void mNUMBER() throws RecognitionException {
		try {
			int _type = NUMBER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:409:7: ( ( '0' .. '9' )+ ( '.' ( '0' .. '9' )* )? )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:410:3: ( '0' .. '9' )+ ( '.' ( '0' .. '9' )* )?
			{
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:410:3: ( '0' .. '9' )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:410:14: ( '.' ( '0' .. '9' )* )?
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0=='.') ) {
				alt5=1;
			}
			switch (alt5) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:410:15: '.' ( '0' .. '9' )*
					{
					match('.'); 
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:410:18: ( '0' .. '9' )*
					loop4:
					while (true) {
						int alt4=2;
						int LA4_0 = input.LA(1);
						if ( ((LA4_0 >= '0' && LA4_0 <= '9')) ) {
							alt4=1;
						}

						switch (alt4) {
						case 1 :
							// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop4;
						}
					}

					}
					break;

			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NUMBER"

	// $ANTLR start "LOG_SIGN"
	public final void mLOG_SIGN() throws RecognitionException {
		try {
			int _type = LOG_SIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:412:9: ( '<' | '>' | '==' | '!=' | '<=' | '>=' )
			int alt6=6;
			switch ( input.LA(1) ) {
			case '<':
				{
				int LA6_1 = input.LA(2);
				if ( (LA6_1=='=') ) {
					alt6=5;
				}

				else {
					alt6=1;
				}

				}
				break;
			case '>':
				{
				int LA6_2 = input.LA(2);
				if ( (LA6_2=='=') ) {
					alt6=6;
				}

				else {
					alt6=2;
				}

				}
				break;
			case '=':
				{
				alt6=3;
				}
				break;
			case '!':
				{
				alt6=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}
			switch (alt6) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:413:3: '<'
					{
					match('<'); 
					}
					break;
				case 2 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:413:7: '>'
					{
					match('>'); 
					}
					break;
				case 3 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:413:11: '=='
					{
					match("=="); 

					}
					break;
				case 4 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:413:16: '!='
					{
					match("!="); 

					}
					break;
				case 5 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:413:21: '<='
					{
					match("<="); 

					}
					break;
				case 6 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:413:26: '>='
					{
					match(">="); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LOG_SIGN"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:415:8: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' | '/*' ( options {greedy=false; } : . )* '*/' )
			int alt10=2;
			int LA10_0 = input.LA(1);
			if ( (LA10_0=='/') ) {
				int LA10_1 = input.LA(2);
				if ( (LA10_1=='/') ) {
					alt10=1;
				}
				else if ( (LA10_1=='*') ) {
					alt10=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 10, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 10, 0, input);
				throw nvae;
			}

			switch (alt10) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:416:3: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
					{
					match("//"); 

					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:416:8: (~ ( '\\n' | '\\r' ) )*
					loop7:
					while (true) {
						int alt7=2;
						int LA7_0 = input.LA(1);
						if ( ((LA7_0 >= '\u0000' && LA7_0 <= '\t')||(LA7_0 >= '\u000B' && LA7_0 <= '\f')||(LA7_0 >= '\u000E' && LA7_0 <= '\uFFFF')) ) {
							alt7=1;
						}

						switch (alt7) {
						case 1 :
							// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:
							{
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop7;
						}
					}

					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:416:22: ( '\\r' )?
					int alt8=2;
					int LA8_0 = input.LA(1);
					if ( (LA8_0=='\r') ) {
						alt8=1;
					}
					switch (alt8) {
						case 1 :
							// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:416:22: '\\r'
							{
							match('\r'); 
							}
							break;

					}

					match('\n'); 
					_channel=HIDDEN;
					}
					break;
				case 2 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:417:5: '/*' ( options {greedy=false; } : . )* '*/'
					{
					match("/*"); 

					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:417:10: ( options {greedy=false; } : . )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( (LA9_0=='*') ) {
							int LA9_1 = input.LA(2);
							if ( (LA9_1=='/') ) {
								alt9=2;
							}
							else if ( ((LA9_1 >= '\u0000' && LA9_1 <= '.')||(LA9_1 >= '0' && LA9_1 <= '\uFFFF')) ) {
								alt9=1;
							}

						}
						else if ( ((LA9_0 >= '\u0000' && LA9_0 <= ')')||(LA9_0 >= '+' && LA9_0 <= '\uFFFF')) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:417:38: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop9;
						}
					}

					match("*/"); 

					_channel=HIDDEN;
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:419:3: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:420:3: ( ' ' | '\\t' | '\\r' | '\\n' )+
			{
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:420:3: ( ' ' | '\\t' | '\\r' | '\\n' )+
			int cnt11=0;
			loop11:
			while (true) {
				int alt11=2;
				int LA11_0 = input.LA(1);
				if ( ((LA11_0 >= '\t' && LA11_0 <= '\n')||LA11_0=='\r'||LA11_0==' ') ) {
					alt11=1;
				}

				switch (alt11) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt11 >= 1 ) break loop11;
					EarlyExitException eee = new EarlyExitException(11, input);
					throw eee;
				}
				cnt11++;
			}

			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	@Override
	public void mTokens() throws RecognitionException {
		// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:8: ( AND | ASSIGN | BOR | COLON | COLUMN | COMMA | CONJ | DEFINE | DISJ | DIV | ELSE | END | FOR | FUN_PRINT | IF | L_BR | L_CBR | L_RBR | MAIN | MINUS | MULT | NOT | OR | PLUS | POINT | PUTS | QUOTE | REPEAT | RETURN | ROW | R_BR | R_CBR | R_RBR | SEMI | SIZE | UNDEFINED | UNTIL | VAR | WHILE | ID | STRING | NUMBER | LOG_SIGN | COMMENT | WS )
		int alt12=45;
		alt12 = dfa12.predict(input);
		switch (alt12) {
			case 1 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:10: AND
				{
				mAND(); 

				}
				break;
			case 2 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:14: ASSIGN
				{
				mASSIGN(); 

				}
				break;
			case 3 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:21: BOR
				{
				mBOR(); 

				}
				break;
			case 4 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:25: COLON
				{
				mCOLON(); 

				}
				break;
			case 5 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:31: COLUMN
				{
				mCOLUMN(); 

				}
				break;
			case 6 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:38: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 7 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:44: CONJ
				{
				mCONJ(); 

				}
				break;
			case 8 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:49: DEFINE
				{
				mDEFINE(); 

				}
				break;
			case 9 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:56: DISJ
				{
				mDISJ(); 

				}
				break;
			case 10 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:61: DIV
				{
				mDIV(); 

				}
				break;
			case 11 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:65: ELSE
				{
				mELSE(); 

				}
				break;
			case 12 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:70: END
				{
				mEND(); 

				}
				break;
			case 13 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:74: FOR
				{
				mFOR(); 

				}
				break;
			case 14 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:78: FUN_PRINT
				{
				mFUN_PRINT(); 

				}
				break;
			case 15 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:88: IF
				{
				mIF(); 

				}
				break;
			case 16 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:91: L_BR
				{
				mL_BR(); 

				}
				break;
			case 17 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:96: L_CBR
				{
				mL_CBR(); 

				}
				break;
			case 18 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:102: L_RBR
				{
				mL_RBR(); 

				}
				break;
			case 19 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:108: MAIN
				{
				mMAIN(); 

				}
				break;
			case 20 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:113: MINUS
				{
				mMINUS(); 

				}
				break;
			case 21 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:119: MULT
				{
				mMULT(); 

				}
				break;
			case 22 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:124: NOT
				{
				mNOT(); 

				}
				break;
			case 23 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:128: OR
				{
				mOR(); 

				}
				break;
			case 24 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:131: PLUS
				{
				mPLUS(); 

				}
				break;
			case 25 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:136: POINT
				{
				mPOINT(); 

				}
				break;
			case 26 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:142: PUTS
				{
				mPUTS(); 

				}
				break;
			case 27 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:147: QUOTE
				{
				mQUOTE(); 

				}
				break;
			case 28 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:153: REPEAT
				{
				mREPEAT(); 

				}
				break;
			case 29 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:160: RETURN
				{
				mRETURN(); 

				}
				break;
			case 30 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:167: ROW
				{
				mROW(); 

				}
				break;
			case 31 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:171: R_BR
				{
				mR_BR(); 

				}
				break;
			case 32 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:176: R_CBR
				{
				mR_CBR(); 

				}
				break;
			case 33 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:182: R_RBR
				{
				mR_RBR(); 

				}
				break;
			case 34 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:188: SEMI
				{
				mSEMI(); 

				}
				break;
			case 35 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:193: SIZE
				{
				mSIZE(); 

				}
				break;
			case 36 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:198: UNDEFINED
				{
				mUNDEFINED(); 

				}
				break;
			case 37 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:208: UNTIL
				{
				mUNTIL(); 

				}
				break;
			case 38 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:214: VAR
				{
				mVAR(); 

				}
				break;
			case 39 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:218: WHILE
				{
				mWHILE(); 

				}
				break;
			case 40 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:224: ID
				{
				mID(); 

				}
				break;
			case 41 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:227: STRING
				{
				mSTRING(); 

				}
				break;
			case 42 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:234: NUMBER
				{
				mNUMBER(); 

				}
				break;
			case 43 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:241: LOG_SIGN
				{
				mLOG_SIGN(); 

				}
				break;
			case 44 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:250: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 45 :
				// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:1:258: WS
				{
				mWS(); 

				}
				break;

		}
	}


	protected DFA12 dfa12 = new DFA12(this);
	static final String DFA12_eotS =
		"\1\uffff\1\42\1\47\1\51\1\uffff\1\42\2\uffff\1\42\1\55\4\42\3\uffff\1"+
		"\42\2\uffff\2\42\2\uffff\1\67\1\42\4\uffff\4\42\4\uffff\1\42\3\uffff\2"+
		"\42\2\uffff\5\42\1\107\2\42\1\112\2\uffff\6\42\1\123\1\42\1\125\1\42\1"+
		"\127\1\130\2\42\1\uffff\1\42\1\134\1\uffff\2\42\1\137\3\42\1\143\1\42"+
		"\1\uffff\1\42\1\uffff\1\146\2\uffff\2\42\1\151\1\uffff\2\42\1\uffff\1"+
		"\154\2\42\1\uffff\2\42\1\uffff\1\161\2\uffff\2\42\1\uffff\1\42\1\165\1"+
		"\166\1\167\1\uffff\1\170\1\171\1\42\5\uffff\2\42\1\175\1\uffff";
	static final String DFA12_eofS =
		"\176\uffff";
	static final String DFA12_minS =
		"\1\11\1\156\1\75\1\174\1\uffff\1\157\2\uffff\1\145\1\52\1\154\1\157\1"+
		"\162\1\146\3\uffff\1\141\2\uffff\1\157\1\162\2\uffff\1\0\1\145\4\uffff"+
		"\1\151\1\156\1\141\1\150\4\uffff\1\144\3\uffff\1\154\1\146\2\uffff\1\163"+
		"\1\144\1\162\1\151\1\164\1\60\1\151\1\164\1\60\2\uffff\1\160\1\167\1\172"+
		"\1\144\1\162\1\151\1\60\1\165\1\60\1\145\2\60\1\156\1\163\1\uffff\1\156"+
		"\1\60\1\uffff\1\145\1\165\1\60\2\145\1\151\1\60\1\154\1\uffff\1\155\1"+
		"\uffff\1\60\2\uffff\1\164\1\40\1\60\1\uffff\1\141\1\162\1\uffff\1\60\1"+
		"\146\1\154\1\uffff\1\145\1\156\1\uffff\1\60\2\uffff\1\164\1\156\1\uffff"+
		"\1\151\3\60\1\uffff\2\60\1\156\5\uffff\1\145\1\144\1\60\1\uffff";
	static final String DFA12_maxS =
		"\1\175\1\156\1\75\1\174\1\uffff\1\157\2\uffff\1\145\1\57\1\156\1\157\1"+
		"\165\1\146\3\uffff\1\141\2\uffff\1\157\1\162\2\uffff\1\uffff\1\157\4\uffff"+
		"\1\151\1\156\1\141\1\150\4\uffff\1\144\3\uffff\1\154\1\146\2\uffff\1\163"+
		"\1\144\1\162\1\151\1\164\1\172\1\151\1\164\1\172\2\uffff\1\164\1\167\1"+
		"\172\1\164\1\162\1\151\1\172\1\165\1\172\1\145\2\172\1\156\1\163\1\uffff"+
		"\1\156\1\172\1\uffff\1\145\1\165\1\172\2\145\1\151\1\172\1\154\1\uffff"+
		"\1\155\1\uffff\1\172\2\uffff\1\164\1\40\1\172\1\uffff\1\141\1\162\1\uffff"+
		"\1\172\1\146\1\154\1\uffff\1\145\1\156\1\uffff\1\172\2\uffff\1\164\1\156"+
		"\1\uffff\1\151\3\172\1\uffff\2\172\1\156\5\uffff\1\145\1\144\1\172\1\uffff";
	static final String DFA12_acceptS =
		"\4\uffff\1\4\1\uffff\1\6\1\7\6\uffff\1\20\1\21\1\22\1\uffff\1\24\1\25"+
		"\2\uffff\1\30\1\31\2\uffff\1\37\1\40\1\41\1\42\4\uffff\1\50\1\52\1\53"+
		"\1\55\1\uffff\1\2\1\11\1\3\2\uffff\1\54\1\12\11\uffff\1\33\1\51\16\uffff"+
		"\1\17\2\uffff\1\27\10\uffff\1\1\1\uffff\1\10\1\uffff\1\14\1\15\3\uffff"+
		"\1\26\2\uffff\1\36\3\uffff\1\46\2\uffff\1\13\1\uffff\1\32\1\23\2\uffff"+
		"\1\43\4\uffff\1\16\3\uffff\1\45\1\47\1\5\1\34\1\35\3\uffff\1\44";
	static final String DFA12_specialS =
		"\30\uffff\1\0\145\uffff}>";
	static final String[] DFA12_transitionS = {
			"\2\45\2\uffff\1\45\22\uffff\1\45\1\44\1\30\3\uffff\1\7\1\uffff\1\20\1"+
			"\34\1\23\1\26\1\6\1\22\1\27\1\11\12\43\1\4\1\35\1\44\1\2\1\44\2\uffff"+
			"\14\42\1\21\15\42\1\16\1\uffff\1\32\1\uffff\1\42\1\uffff\1\1\1\42\1\5"+
			"\1\10\1\12\1\13\2\42\1\15\4\42\1\24\1\25\1\14\1\42\1\31\1\36\1\42\1\37"+
			"\1\40\1\41\3\42\1\17\1\3\1\33",
			"\1\46",
			"\1\44",
			"\1\50",
			"",
			"\1\52",
			"",
			"",
			"\1\53",
			"\1\54\4\uffff\1\54",
			"\1\56\1\uffff\1\57",
			"\1\60",
			"\1\61\2\uffff\1\62",
			"\1\63",
			"",
			"",
			"",
			"\1\64",
			"",
			"",
			"\1\65",
			"\1\66",
			"",
			"",
			"\0\70",
			"\1\71\11\uffff\1\72",
			"",
			"",
			"",
			"",
			"\1\73",
			"\1\74",
			"\1\75",
			"\1\76",
			"",
			"",
			"",
			"",
			"\1\77",
			"",
			"",
			"",
			"\1\100",
			"\1\101",
			"",
			"",
			"\1\102",
			"\1\103",
			"\1\104",
			"\1\105",
			"\1\106",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\1\110",
			"\1\111",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"",
			"",
			"\1\113\3\uffff\1\114",
			"\1\115",
			"\1\116",
			"\1\117\17\uffff\1\120",
			"\1\121",
			"\1\122",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\1\124",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\1\126",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\1\131",
			"\1\132",
			"",
			"\1\133",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"",
			"\1\135",
			"\1\136",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\1\140",
			"\1\141",
			"\1\142",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\1\144",
			"",
			"\1\145",
			"",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"",
			"",
			"\1\147",
			"\1\150",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"",
			"\1\152",
			"\1\153",
			"",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\1\155",
			"\1\156",
			"",
			"\1\157",
			"\1\160",
			"",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"",
			"",
			"\1\162",
			"\1\163",
			"",
			"\1\164",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			"\1\172",
			"",
			"",
			"",
			"",
			"",
			"\1\173",
			"\1\174",
			"\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
			""
	};

	static final short[] DFA12_eot = DFA.unpackEncodedString(DFA12_eotS);
	static final short[] DFA12_eof = DFA.unpackEncodedString(DFA12_eofS);
	static final char[] DFA12_min = DFA.unpackEncodedStringToUnsignedChars(DFA12_minS);
	static final char[] DFA12_max = DFA.unpackEncodedStringToUnsignedChars(DFA12_maxS);
	static final short[] DFA12_accept = DFA.unpackEncodedString(DFA12_acceptS);
	static final short[] DFA12_special = DFA.unpackEncodedString(DFA12_specialS);
	static final short[][] DFA12_transition;

	static {
		int numStates = DFA12_transitionS.length;
		DFA12_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA12_transition[i] = DFA.unpackEncodedString(DFA12_transitionS[i]);
		}
	}

	protected class DFA12 extends DFA {

		public DFA12(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 12;
			this.eot = DFA12_eot;
			this.eof = DFA12_eof;
			this.min = DFA12_min;
			this.max = DFA12_max;
			this.accept = DFA12_accept;
			this.special = DFA12_special;
			this.transition = DFA12_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( AND | ASSIGN | BOR | COLON | COLUMN | COMMA | CONJ | DEFINE | DISJ | DIV | ELSE | END | FOR | FUN_PRINT | IF | L_BR | L_CBR | L_RBR | MAIN | MINUS | MULT | NOT | OR | PLUS | POINT | PUTS | QUOTE | REPEAT | RETURN | ROW | R_BR | R_CBR | R_RBR | SEMI | SIZE | UNDEFINED | UNTIL | VAR | WHILE | ID | STRING | NUMBER | LOG_SIGN | COMMENT | WS );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA12_24 = input.LA(1);
						s = -1;
						if ( ((LA12_24 >= '\u0000' && LA12_24 <= '\uFFFF')) ) {s = 56;}
						else s = 55;
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 12, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
