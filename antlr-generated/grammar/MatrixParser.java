// $ANTLR 3.5.2 G:\\wsi2\\MatrixCompiler\\src\\Matrix.g 2016-09-06 01:13:01

package grammar;
    import block.*;
    import error.*;
    import function.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.stringtemplate.*;
import org.antlr.stringtemplate.language.*;
import java.util.HashMap;
@SuppressWarnings("all")
public class MatrixParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ASSIGN", "BOR", "COLON", 
		"COLUMN", "COMMA", "COMMENT", "CONJ", "DEFINE", "DISJ", "DIV", "ELSE", 
		"END", "FOR", "FUN_PRINT", "ID", "IF", "LOG_SIGN", "L_BR", "L_CBR", "L_RBR", 
		"MAIN", "MINUS", "MULT", "NOT", "NUMBER", "OR", "PLUS", "POINT", "PUTS", 
		"QUOTE", "REPEAT", "RETURN", "ROW", "R_BR", "R_CBR", "R_RBR", "SEMI", 
		"SIZE", "STRING", "UNDEFINED", "UNTIL", "VAR", "WHILE", "WS"
	};
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ASSIGN=5;
	public static final int BOR=6;
	public static final int COLON=7;
	public static final int COLUMN=8;
	public static final int COMMA=9;
	public static final int COMMENT=10;
	public static final int CONJ=11;
	public static final int DEFINE=12;
	public static final int DISJ=13;
	public static final int DIV=14;
	public static final int ELSE=15;
	public static final int END=16;
	public static final int FOR=17;
	public static final int FUN_PRINT=18;
	public static final int ID=19;
	public static final int IF=20;
	public static final int LOG_SIGN=21;
	public static final int L_BR=22;
	public static final int L_CBR=23;
	public static final int L_RBR=24;
	public static final int MAIN=25;
	public static final int MINUS=26;
	public static final int MULT=27;
	public static final int NOT=28;
	public static final int NUMBER=29;
	public static final int OR=30;
	public static final int PLUS=31;
	public static final int POINT=32;
	public static final int PUTS=33;
	public static final int QUOTE=34;
	public static final int REPEAT=35;
	public static final int RETURN=36;
	public static final int ROW=37;
	public static final int R_BR=38;
	public static final int R_CBR=39;
	public static final int R_RBR=40;
	public static final int SEMI=41;
	public static final int SIZE=42;
	public static final int STRING=43;
	public static final int UNDEFINED=44;
	public static final int UNTIL=45;
	public static final int VAR=46;
	public static final int WHILE=47;
	public static final int WS=48;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators

	protected static class slist_scope {
		List locals;
		List stats;
	}
	protected Stack<slist_scope> slist_stack = new Stack<slist_scope>();


	public MatrixParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public MatrixParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected StringTemplateGroup templateLib =
	  new StringTemplateGroup("MatrixParserTemplates", AngleBracketTemplateLexer.class);

	public void setTemplateLib(StringTemplateGroup templateLib) {
	  this.templateLib = templateLib;
	}
	public StringTemplateGroup getTemplateLib() {
	  return templateLib;
	}
	/** allows convenient multi-value initialization:
	 *  "new STAttrMap().put(...).put(...)"
	 */
	@SuppressWarnings("serial")
	public static class STAttrMap extends HashMap<String, Object> {
		public STAttrMap put(String attrName, Object value) {
			super.put(attrName, value);
			return this;
		}
	}
	@Override public String[] getTokenNames() { return MatrixParser.tokenNames; }
	@Override public String getGrammarFileName() { return "G:\\wsi2\\MatrixCompiler\\src\\Matrix.g"; }


		Blocks blocks = new Blocks();
		Errors errors = new Errors();
		Functions functions = new Functions();

		// for string templates
		public String toString(String str){
		  return "\"" + str + "\"";
	  }


	public static class program_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "program"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:80:1: program : (funcs+= functionDeclaration )* MAIN COLON (stats+= statement )* END -> program(funcs=$funcsstats=$stats);
	public final MatrixParser.program_return program() throws RecognitionException {
		MatrixParser.program_return retval = new MatrixParser.program_return();
		retval.start = input.LT(1);

		List<Object> list_funcs=null;
		List<Object> list_stats=null;
		RuleReturnScope funcs = null;
		RuleReturnScope stats = null;
		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:80:8: ( (funcs+= functionDeclaration )* MAIN COLON (stats+= statement )* END -> program(funcs=$funcsstats=$stats))
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:81:3: (funcs+= functionDeclaration )* MAIN COLON (stats+= statement )* END
			{
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:81:3: (funcs+= functionDeclaration )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==DEFINE) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:81:4: funcs+= functionDeclaration
					{
					pushFollow(FOLLOW_functionDeclaration_in_program679);
					funcs=functionDeclaration();
					state._fsp--;

					if (list_funcs==null) list_funcs=new ArrayList<Object>();
					list_funcs.add(funcs.getTemplate());
					}
					break;

				default :
					break loop1;
				}
			}

			match(input,MAIN,FOLLOW_MAIN_in_program685); 
			match(input,COLON,FOLLOW_COLON_in_program687); 

					 blocks.addBlock();
					
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:86:5: (stats+= statement )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( (LA2_0==FOR||(LA2_0 >= ID && LA2_0 <= IF)||LA2_0==PUTS||LA2_0==REPEAT||(LA2_0 >= VAR && LA2_0 <= WHILE)) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:86:6: stats+= statement
					{
					pushFollow(FOLLOW_statement_in_program704);
					stats=statement();
					state._fsp--;

					if (list_stats==null) list_stats=new ArrayList<Object>();
					list_stats.add(stats.getTemplate());
					}
					break;

				default :
					break loop2;
				}
			}


					 blocks.removeBlock();
					
			match(input,END,FOLLOW_END_in_program714); 

			   errors.printErrors();
			  
			// TEMPLATE REWRITE
			// 94:3: -> program(funcs=$funcsstats=$stats)
			{
				retval.st = templateLib.getInstanceOf("program",new STAttrMap().put("funcs", list_funcs).put("stats", list_stats));
			}



			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "program"


	protected static class functionDeclaration_scope {
		List<String> params;
	}
	protected Stack<functionDeclaration_scope> functionDeclaration_stack = new Stack<functionDeclaration_scope>();

	public static class functionDeclaration_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "functionDeclaration"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:97:1: functionDeclaration : DEFINE name= ID ( L_RBR (p= ID ( COMMA p= ID )* )? R_RBR )? COLON (stats+= statement )* ( RETURN e= expression )? END ;
	public final MatrixParser.functionDeclaration_return functionDeclaration() throws RecognitionException {
		functionDeclaration_stack.push(new functionDeclaration_scope());
		MatrixParser.functionDeclaration_return retval = new MatrixParser.functionDeclaration_return();
		retval.start = input.LT(1);

		Token name=null;
		Token p=null;
		Token L_RBR1=null;
		List<Object> list_stats=null;
		ParserRuleReturnScope e =null;
		RuleReturnScope stats = null;

		  functionDeclaration_stack.peek().params = new ArrayList<String>();

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:103:2: ( DEFINE name= ID ( L_RBR (p= ID ( COMMA p= ID )* )? R_RBR )? COLON (stats+= statement )* ( RETURN e= expression )? END )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:104:3: DEFINE name= ID ( L_RBR (p= ID ( COMMA p= ID )* )? R_RBR )? COLON (stats+= statement )* ( RETURN e= expression )? END
			{
			match(input,DEFINE,FOLLOW_DEFINE_in_functionDeclaration759); 
			name=(Token)match(input,ID,FOLLOW_ID_in_functionDeclaration763); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:104:18: ( L_RBR (p= ID ( COMMA p= ID )* )? R_RBR )?
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==L_RBR) ) {
				alt5=1;
			}
			switch (alt5) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:104:19: L_RBR (p= ID ( COMMA p= ID )* )? R_RBR
					{
					L_RBR1=(Token)match(input,L_RBR,FOLLOW_L_RBR_in_functionDeclaration766); 
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:104:25: (p= ID ( COMMA p= ID )* )?
					int alt4=2;
					int LA4_0 = input.LA(1);
					if ( (LA4_0==ID) ) {
						alt4=1;
					}
					switch (alt4) {
						case 1 :
							// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:104:26: p= ID ( COMMA p= ID )*
							{
							p=(Token)match(input,ID,FOLLOW_ID_in_functionDeclaration771); 
							functionDeclaration_stack.peek().params.add((p!=null?p.getText():null));
							// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:105:19: ( COMMA p= ID )*
							loop3:
							while (true) {
								int alt3=2;
								int LA3_0 = input.LA(1);
								if ( (LA3_0==COMMA) ) {
									alt3=1;
								}

								switch (alt3) {
								case 1 :
									// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:105:20: COMMA p= ID
									{
									match(input,COMMA,FOLLOW_COMMA_in_functionDeclaration793); 
									p=(Token)match(input,ID,FOLLOW_ID_in_functionDeclaration797); 
									functionDeclaration_stack.peek().params.add((p!=null?p.getText():null));
									}
									break;

								default :
									break loop3;
								}
							}

							}
							break;

					}

					match(input,R_RBR,FOLLOW_R_RBR_in_functionDeclaration804); 
					}
					break;

			}

			match(input,COLON,FOLLOW_COLON_in_functionDeclaration808); 

			     blocks.addBlock();
			     for(String id: functionDeclaration_stack.peek().params) {
			        blocks.addVar(id,(L_RBR1!=null?L_RBR1.getLine():0));
			     }
			     functions.addFunc((name!=null?name.getText():null),functionDeclaration_stack.peek().params);
			    
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:113:3: (stats+= statement )*
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0==FOR||(LA6_0 >= ID && LA6_0 <= IF)||LA6_0==PUTS||LA6_0==REPEAT||(LA6_0 >= VAR && LA6_0 <= WHILE)) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:113:4: stats+= statement
					{
					pushFollow(FOLLOW_statement_in_functionDeclaration823);
					stats=statement();
					state._fsp--;

					if (list_stats==null) list_stats=new ArrayList<Object>();
					list_stats.add(stats.getTemplate());
					}
					break;

				default :
					break loop6;
				}
			}

			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:114:3: ( RETURN e= expression )?
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0==RETURN) ) {
				alt7=1;
			}
			switch (alt7) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:114:4: RETURN e= expression
					{
					match(input,RETURN,FOLLOW_RETURN_in_functionDeclaration830); 
					pushFollow(FOLLOW_expression_in_functionDeclaration834);
					e=expression();
					state._fsp--;

					}
					break;

			}


					 blocks.removeBlock();
					 if(functionDeclaration_stack.peek().params.size() > 0) {
			        String firstParam = functionDeclaration_stack.peek().params.get(0);
			        functionDeclaration_stack.peek().params.set(0, "Variable " + firstParam);
			     }
					 if((e!=null?((StringTemplate)e.getTemplate()):null) != null) {
					    retval.st = templateLib.getInstanceOf("functionDeclaration",new STAttrMap().put("name", (name!=null?name.getText():null)).put("params", functionDeclaration_stack.peek().params).put("returned", (e!=null?((StringTemplate)e.getTemplate()):null)).put("stats", list_stats));
					 } else {
					    retval.st = templateLib.getInstanceOf("voidFunctionDeclaration",new STAttrMap().put("name", (name!=null?name.getText():null)).put("params", functionDeclaration_stack.peek().params).put("stats", list_stats));
					 }
					
			match(input,END,FOLLOW_END_in_functionDeclaration844); 
			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
			functionDeclaration_stack.pop();
		}
		return retval;
	}
	// $ANTLR end "functionDeclaration"


	public static class functionCall_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "functionCall"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:130:1: functionCall : name= ID L_RBR (p+= expression ( COMMA p+= expression )* )? R_RBR -> functionCall(name=$name.textparams=$p);
	public final MatrixParser.functionCall_return functionCall() throws RecognitionException {
		MatrixParser.functionCall_return retval = new MatrixParser.functionCall_return();
		retval.start = input.LT(1);

		Token name=null;
		List<Object> list_p=null;
		RuleReturnScope p = null;
		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:130:13: (name= ID L_RBR (p+= expression ( COMMA p+= expression )* )? R_RBR -> functionCall(name=$name.textparams=$p))
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:131:3: name= ID L_RBR (p+= expression ( COMMA p+= expression )* )? R_RBR
			{
			name=(Token)match(input,ID,FOLLOW_ID_in_functionCall858); 
			match(input,L_RBR,FOLLOW_L_RBR_in_functionCall860); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:131:17: (p+= expression ( COMMA p+= expression )* )?
			int alt9=2;
			int LA9_0 = input.LA(1);
			if ( (LA9_0==ID||(LA9_0 >= L_BR && LA9_0 <= L_RBR)||LA9_0==MINUS||LA9_0==NUMBER||LA9_0==UNDEFINED) ) {
				alt9=1;
			}
			switch (alt9) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:131:18: p+= expression ( COMMA p+= expression )*
					{
					pushFollow(FOLLOW_expression_in_functionCall865);
					p=expression();
					state._fsp--;

					if (list_p==null) list_p=new ArrayList<Object>();
					list_p.add(p.getTemplate());
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:132:11: ( COMMA p+= expression )*
					loop8:
					while (true) {
						int alt8=2;
						int LA8_0 = input.LA(1);
						if ( (LA8_0==COMMA) ) {
							alt8=1;
						}

						switch (alt8) {
						case 1 :
							// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:132:12: COMMA p+= expression
							{
							match(input,COMMA,FOLLOW_COMMA_in_functionCall878); 
							pushFollow(FOLLOW_expression_in_functionCall882);
							p=expression();
							state._fsp--;

							if (list_p==null) list_p=new ArrayList<Object>();
							list_p.add(p.getTemplate());
							}
							break;

						default :
							break loop8;
						}
					}

					}
					break;

			}

			match(input,R_RBR,FOLLOW_R_RBR_in_functionCall888); 

			   if(!functions.containsFunc((name!=null?name.getText():null))){
			      errors.addError("Undefined method " + (name!=null?name.getText():null) + ".",(name!=null?name.getLine():0));
			   }
			  
			// TEMPLATE REWRITE
			// 138:3: -> functionCall(name=$name.textparams=$p)
			{
				retval.st = templateLib.getInstanceOf("functionCall",new STAttrMap().put("name", (name!=null?name.getText():null)).put("params", list_p));
			}



			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "functionCall"


	public static class expression_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "expression"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:141:1: expression :opd+= subexpr (op= ( PLUS | MINUS | MULT | DIV ) opd+= subexpr )? ;
	public final MatrixParser.expression_return expression() throws RecognitionException {
		MatrixParser.expression_return retval = new MatrixParser.expression_return();
		retval.start = input.LT(1);

		Token op=null;
		List<Object> list_opd=null;
		RuleReturnScope opd = null;
		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:141:11: (opd+= subexpr (op= ( PLUS | MINUS | MULT | DIV ) opd+= subexpr )? )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:142:3: opd+= subexpr (op= ( PLUS | MINUS | MULT | DIV ) opd+= subexpr )?
			{
			pushFollow(FOLLOW_subexpr_in_expression926);
			opd=subexpr();
			state._fsp--;

			if (list_opd==null) list_opd=new ArrayList<Object>();
			list_opd.add(opd.getTemplate());
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:142:16: (op= ( PLUS | MINUS | MULT | DIV ) opd+= subexpr )?
			int alt10=2;
			int LA10_0 = input.LA(1);
			if ( (LA10_0==DIV||(LA10_0 >= MINUS && LA10_0 <= MULT)||LA10_0==PLUS) ) {
				alt10=1;
			}
			switch (alt10) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:142:17: op= ( PLUS | MINUS | MULT | DIV ) opd+= subexpr
					{
					op=input.LT(1);
					if ( input.LA(1)==DIV||(input.LA(1) >= MINUS && input.LA(1) <= MULT)||input.LA(1)==PLUS ) {
						input.consume();
						state.errorRecovery=false;
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					pushFollow(FOLLOW_subexpr_in_expression943);
					opd=subexpr();
					state._fsp--;

					if (list_opd==null) list_opd=new ArrayList<Object>();
					list_opd.add(opd.getTemplate());
					}
					break;

			}


			   if(op != null) {
			      switch((op!=null?op.getText():null)) {
			        case "+":
			          retval.st = templateLib.getInstanceOf("plus_template",new STAttrMap().put("operands", list_opd));
			          break;
			        case "-":
			          retval.st = templateLib.getInstanceOf("minus_template",new STAttrMap().put("operands", list_opd));
			          break;
			        case "*":
			          retval.st = templateLib.getInstanceOf("mult_template",new STAttrMap().put("operands", list_opd));
			          break;
			        case "/":
			          retval.st = templateLib.getInstanceOf("div_template",new STAttrMap().put("operands", list_opd));
			          break;
			      }
			   } else retval.st = templateLib.getInstanceOf("atom_template",new STAttrMap().put("operands", list_opd));
			  
			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expression"


	public static class subexpr_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "subexpr"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:163:1: subexpr : ( functionCall -> {$functionCall.st;}| L_RBR expression R_RBR -> {$expression.st;}| ID -> {new StringTemplate($ID.text);}| number -> initNumber(val=$number.st)| initializationVector -> {$initializationVector.st}| initializationMatrix -> {$initializationMatrix.st}| getter -> {$getter.st}| sizeRowColumn -> {$sizeRowColumn.st}| UNDEFINED -> undefined();
	public final MatrixParser.subexpr_return subexpr() throws RecognitionException {
		MatrixParser.subexpr_return retval = new MatrixParser.subexpr_return();
		retval.start = input.LT(1);

		Token ID4=null;
		ParserRuleReturnScope functionCall2 =null;
		ParserRuleReturnScope expression3 =null;
		ParserRuleReturnScope number5 =null;
		ParserRuleReturnScope initializationVector6 =null;
		ParserRuleReturnScope initializationMatrix7 =null;
		ParserRuleReturnScope getter8 =null;
		ParserRuleReturnScope sizeRowColumn9 =null;

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:163:8: ( functionCall -> {$functionCall.st;}| L_RBR expression R_RBR -> {$expression.st;}| ID -> {new StringTemplate($ID.text);}| number -> initNumber(val=$number.st)| initializationVector -> {$initializationVector.st}| initializationMatrix -> {$initializationMatrix.st}| getter -> {$getter.st}| sizeRowColumn -> {$sizeRowColumn.st}| UNDEFINED -> undefined()
			int alt11=9;
			switch ( input.LA(1) ) {
			case ID:
				{
				switch ( input.LA(2) ) {
				case L_RBR:
					{
					alt11=1;
					}
					break;
				case L_BR:
					{
					alt11=7;
					}
					break;
				case POINT:
					{
					alt11=8;
					}
					break;
				case COMMA:
				case CONJ:
				case DISJ:
				case DIV:
				case ELSE:
				case END:
				case FOR:
				case ID:
				case IF:
				case LOG_SIGN:
				case MINUS:
				case MULT:
				case PLUS:
				case PUTS:
				case REPEAT:
				case RETURN:
				case R_BR:
				case R_RBR:
				case SEMI:
				case UNTIL:
				case VAR:
				case WHILE:
					{
					alt11=3;
					}
					break;
				default:
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 11, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
				}
				break;
			case L_RBR:
				{
				alt11=2;
				}
				break;
			case MINUS:
			case NUMBER:
				{
				alt11=4;
				}
				break;
			case L_BR:
				{
				alt11=5;
				}
				break;
			case L_CBR:
				{
				alt11=6;
				}
				break;
			case UNDEFINED:
				{
				alt11=9;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}
			switch (alt11) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:164:3: functionCall
					{
					pushFollow(FOLLOW_functionCall_in_subexpr961);
					functionCall2=functionCall();
					state._fsp--;

					// TEMPLATE REWRITE
					// 165:3: -> {$functionCall.st;}
					{
						retval.st = (functionCall2!=null?((StringTemplate)functionCall2.getTemplate()):null);;
					}



					}
					break;
				case 2 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:167:3: L_RBR expression R_RBR
					{
					match(input,L_RBR,FOLLOW_L_RBR_in_subexpr975); 
					pushFollow(FOLLOW_expression_in_subexpr977);
					expression3=expression();
					state._fsp--;

					match(input,R_RBR,FOLLOW_R_RBR_in_subexpr979); 
					// TEMPLATE REWRITE
					// 168:3: -> {$expression.st;}
					{
						retval.st = (expression3!=null?((StringTemplate)expression3.getTemplate()):null);;
					}



					}
					break;
				case 3 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:170:3: ID
					{
					ID4=(Token)match(input,ID,FOLLOW_ID_in_subexpr993); 

					   if(!blocks.containsVarInAllBlocks((ID4!=null?ID4.getText():null))) {
					      errors.addError("Not defined variable " + (ID4!=null?ID4.getText():null) + ".", (ID4!=null?ID4.getLine():0));
					   }
					  
					// TEMPLATE REWRITE
					// 176:3: -> {new StringTemplate($ID.text);}
					{
						retval.st = new StringTemplate((ID4!=null?ID4.getText():null));;
					}



					}
					break;
				case 4 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:178:3: number
					{
					pushFollow(FOLLOW_number_in_subexpr1011);
					number5=number();
					state._fsp--;

					// TEMPLATE REWRITE
					// 179:3: -> initNumber(val=$number.st)
					{
						retval.st = templateLib.getInstanceOf("initNumber",new STAttrMap().put("val", (number5!=null?((StringTemplate)number5.getTemplate()):null)));
					}



					}
					break;
				case 5 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:181:3: initializationVector
					{
					pushFollow(FOLLOW_initializationVector_in_subexpr1032);
					initializationVector6=initializationVector();
					state._fsp--;

					// TEMPLATE REWRITE
					// 182:3: -> {$initializationVector.st}
					{
						retval.st = (initializationVector6!=null?((StringTemplate)initializationVector6.getTemplate()):null);
					}



					}
					break;
				case 6 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:184:3: initializationMatrix
					{
					pushFollow(FOLLOW_initializationMatrix_in_subexpr1046);
					initializationMatrix7=initializationMatrix();
					state._fsp--;

					// TEMPLATE REWRITE
					// 185:3: -> {$initializationMatrix.st}
					{
						retval.st = (initializationMatrix7!=null?((StringTemplate)initializationMatrix7.getTemplate()):null);
					}



					}
					break;
				case 7 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:187:3: getter
					{
					pushFollow(FOLLOW_getter_in_subexpr1060);
					getter8=getter();
					state._fsp--;

					// TEMPLATE REWRITE
					// 188:3: -> {$getter.st}
					{
						retval.st = (getter8!=null?((StringTemplate)getter8.getTemplate()):null);
					}



					}
					break;
				case 8 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:190:3: sizeRowColumn
					{
					pushFollow(FOLLOW_sizeRowColumn_in_subexpr1074);
					sizeRowColumn9=sizeRowColumn();
					state._fsp--;

					// TEMPLATE REWRITE
					// 191:3: -> {$sizeRowColumn.st}
					{
						retval.st = (sizeRowColumn9!=null?((StringTemplate)sizeRowColumn9.getTemplate()):null);
					}



					}
					break;
				case 9 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:193:3: UNDEFINED
					{
					match(input,UNDEFINED,FOLLOW_UNDEFINED_in_subexpr1088); 
					// TEMPLATE REWRITE
					// 194:3: -> undefined(
					{
						retval.st = templateLib.getInstanceOf("undefined");
					}



					}
					break;

			}
			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "subexpr"


	public static class sizeRowColumn_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "sizeRowColumn"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:197:1: sizeRowColumn : ID POINT what= ( SIZE | ROW | COLUMN ) ;
	public final MatrixParser.sizeRowColumn_return sizeRowColumn() throws RecognitionException {
		MatrixParser.sizeRowColumn_return retval = new MatrixParser.sizeRowColumn_return();
		retval.start = input.LT(1);

		Token what=null;
		Token ID10=null;

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:197:14: ( ID POINT what= ( SIZE | ROW | COLUMN ) )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:198:3: ID POINT what= ( SIZE | ROW | COLUMN )
			{
			ID10=(Token)match(input,ID,FOLLOW_ID_in_sizeRowColumn1108); 
			match(input,POINT,FOLLOW_POINT_in_sizeRowColumn1110); 
			what=input.LT(1);
			if ( input.LA(1)==COLUMN||input.LA(1)==ROW||input.LA(1)==SIZE ) {
				input.consume();
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}

			   if(!blocks.containsVarInAllBlocks((ID10!=null?ID10.getText():null))) {
			      errors.addError("Undefined variable " + (ID10!=null?ID10.getText():null) + ".",(ID10!=null?ID10.getLine():0));
			   } else {
						switch((what!=null?what.getText():null)) {
						 case "size":
						   retval.st = templateLib.getInstanceOf("size",new STAttrMap().put("name", (ID10!=null?ID10.getText():null)));
						   break;
						 case "row":
						   retval.st = templateLib.getInstanceOf("row",new STAttrMap().put("name", (ID10!=null?ID10.getText():null)));
						   break;
						 case "column":
						   retval.st = templateLib.getInstanceOf("column",new STAttrMap().put("name", (ID10!=null?ID10.getText():null)));
						   break;
						}
			   }
			  
			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "sizeRowColumn"


	public static class getter_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "getter"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:218:1: getter : ID L_BR f= expression R_BR ( L_BR s= expression R_BR )? ;
	public final MatrixParser.getter_return getter() throws RecognitionException {
		MatrixParser.getter_return retval = new MatrixParser.getter_return();
		retval.start = input.LT(1);

		Token ID11=null;
		ParserRuleReturnScope f =null;
		ParserRuleReturnScope s =null;

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:218:7: ( ID L_BR f= expression R_BR ( L_BR s= expression R_BR )? )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:219:3: ID L_BR f= expression R_BR ( L_BR s= expression R_BR )?
			{
			ID11=(Token)match(input,ID,FOLLOW_ID_in_getter1136); 
			match(input,L_BR,FOLLOW_L_BR_in_getter1138); 
			pushFollow(FOLLOW_expression_in_getter1142);
			f=expression();
			state._fsp--;

			match(input,R_BR,FOLLOW_R_BR_in_getter1144); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:219:29: ( L_BR s= expression R_BR )?
			int alt12=2;
			int LA12_0 = input.LA(1);
			if ( (LA12_0==L_BR) ) {
				alt12=1;
			}
			switch (alt12) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:219:30: L_BR s= expression R_BR
					{
					match(input,L_BR,FOLLOW_L_BR_in_getter1147); 
					pushFollow(FOLLOW_expression_in_getter1151);
					s=expression();
					state._fsp--;

					match(input,R_BR,FOLLOW_R_BR_in_getter1153); 
					}
					break;

			}


			   if(!blocks.containsVarInAllBlocks((ID11!=null?ID11.getText():null))) {
			      errors.addError("Undefined variable " + (ID11!=null?ID11.getText():null) + ".",(ID11!=null?ID11.getLine():0));
			   } else if ((s!=null?((StringTemplate)s.getTemplate()):null) != null) {
			      retval.st = templateLib.getInstanceOf("matrixGetter",new STAttrMap().put("var", (ID11!=null?ID11.getText():null)).put("frst", (f!=null?((StringTemplate)f.getTemplate()):null)).put("second", (s!=null?((StringTemplate)s.getTemplate()):null)));
			   } else {
			      retval.st = templateLib.getInstanceOf("vectorGetter",new STAttrMap().put("var", (ID11!=null?ID11.getText():null)).put("frst", (f!=null?((StringTemplate)f.getTemplate()):null)));
			   }
			  
			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "getter"


	public static class statement_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "statement"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:231:1: statement : ( var_define -> statement(st=$var_define.st)| putStat -> statement(st=$putStat.st)| functionCall -> statement(st=$functionCall.st)| whileStat -> {$whileStat.st}| untilStat -> {$untilStat.st}| forStat -> {$forStat.st}| ifElseStat -> {$ifElseStat.st}| setter -> statement(st=$setter.st));
	public final MatrixParser.statement_return statement() throws RecognitionException {
		MatrixParser.statement_return retval = new MatrixParser.statement_return();
		retval.start = input.LT(1);

		ParserRuleReturnScope var_define12 =null;
		ParserRuleReturnScope putStat13 =null;
		ParserRuleReturnScope functionCall14 =null;
		ParserRuleReturnScope whileStat15 =null;
		ParserRuleReturnScope untilStat16 =null;
		ParserRuleReturnScope forStat17 =null;
		ParserRuleReturnScope ifElseStat18 =null;
		ParserRuleReturnScope setter19 =null;

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:231:10: ( var_define -> statement(st=$var_define.st)| putStat -> statement(st=$putStat.st)| functionCall -> statement(st=$functionCall.st)| whileStat -> {$whileStat.st}| untilStat -> {$untilStat.st}| forStat -> {$forStat.st}| ifElseStat -> {$ifElseStat.st}| setter -> statement(st=$setter.st))
			int alt13=8;
			switch ( input.LA(1) ) {
			case VAR:
				{
				alt13=1;
				}
				break;
			case ID:
				{
				switch ( input.LA(2) ) {
				case ASSIGN:
					{
					alt13=1;
					}
					break;
				case L_RBR:
					{
					alt13=3;
					}
					break;
				case L_BR:
					{
					alt13=8;
					}
					break;
				default:
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
				}
				break;
			case PUTS:
				{
				alt13=2;
				}
				break;
			case WHILE:
				{
				alt13=4;
				}
				break;
			case REPEAT:
				{
				alt13=5;
				}
				break;
			case FOR:
				{
				alt13=6;
				}
				break;
			case IF:
				{
				alt13=7;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 13, 0, input);
				throw nvae;
			}
			switch (alt13) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:232:5: var_define
					{
					pushFollow(FOLLOW_var_define_in_statement1173);
					var_define12=var_define();
					state._fsp--;

					// TEMPLATE REWRITE
					// 232:18: -> statement(st=$var_define.st)
					{
						retval.st = templateLib.getInstanceOf("statement",new STAttrMap().put("st", (var_define12!=null?((StringTemplate)var_define12.getTemplate()):null)));
					}



					}
					break;
				case 2 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:233:5: putStat
					{
					pushFollow(FOLLOW_putStat_in_statement1192);
					putStat13=putStat();
					state._fsp--;

					// TEMPLATE REWRITE
					// 233:18: -> statement(st=$putStat.st)
					{
						retval.st = templateLib.getInstanceOf("statement",new STAttrMap().put("st", (putStat13!=null?((StringTemplate)putStat13.getTemplate()):null)));
					}



					}
					break;
				case 3 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:234:5: functionCall
					{
					pushFollow(FOLLOW_functionCall_in_statement1214);
					functionCall14=functionCall();
					state._fsp--;

					// TEMPLATE REWRITE
					// 234:18: -> statement(st=$functionCall.st)
					{
						retval.st = templateLib.getInstanceOf("statement",new STAttrMap().put("st", (functionCall14!=null?((StringTemplate)functionCall14.getTemplate()):null)));
					}



					}
					break;
				case 4 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:235:5: whileStat
					{
					pushFollow(FOLLOW_whileStat_in_statement1231);
					whileStat15=whileStat();
					state._fsp--;

					// TEMPLATE REWRITE
					// 235:18: -> {$whileStat.st}
					{
						retval.st = (whileStat15!=null?((StringTemplate)whileStat15.getTemplate()):null);
					}



					}
					break;
				case 5 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:236:5: untilStat
					{
					pushFollow(FOLLOW_untilStat_in_statement1244);
					untilStat16=untilStat();
					state._fsp--;

					// TEMPLATE REWRITE
					// 236:18: -> {$untilStat.st}
					{
						retval.st = (untilStat16!=null?((StringTemplate)untilStat16.getTemplate()):null);
					}



					}
					break;
				case 6 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:237:5: forStat
					{
					pushFollow(FOLLOW_forStat_in_statement1257);
					forStat17=forStat();
					state._fsp--;

					// TEMPLATE REWRITE
					// 237:18: -> {$forStat.st}
					{
						retval.st = (forStat17!=null?((StringTemplate)forStat17.getTemplate()):null);
					}



					}
					break;
				case 7 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:238:5: ifElseStat
					{
					pushFollow(FOLLOW_ifElseStat_in_statement1272);
					ifElseStat18=ifElseStat();
					state._fsp--;

					// TEMPLATE REWRITE
					// 238:18: -> {$ifElseStat.st}
					{
						retval.st = (ifElseStat18!=null?((StringTemplate)ifElseStat18.getTemplate()):null);
					}



					}
					break;
				case 8 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:239:5: setter
					{
					pushFollow(FOLLOW_setter_in_statement1284);
					setter19=setter();
					state._fsp--;

					// TEMPLATE REWRITE
					// 239:18: -> statement(st=$setter.st)
					{
						retval.st = templateLib.getInstanceOf("statement",new STAttrMap().put("st", (setter19!=null?((StringTemplate)setter19.getTemplate()):null)));
					}



					}
					break;

			}
			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "statement"


	public static class setter_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "setter"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:242:1: setter : ID L_BR f= expression R_BR ( L_BR s= expression R_BR )? ASSIGN val= expression ;
	public final MatrixParser.setter_return setter() throws RecognitionException {
		MatrixParser.setter_return retval = new MatrixParser.setter_return();
		retval.start = input.LT(1);

		Token ID20=null;
		ParserRuleReturnScope f =null;
		ParserRuleReturnScope s =null;
		ParserRuleReturnScope val =null;

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:242:7: ( ID L_BR f= expression R_BR ( L_BR s= expression R_BR )? ASSIGN val= expression )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:243:3: ID L_BR f= expression R_BR ( L_BR s= expression R_BR )? ASSIGN val= expression
			{
			ID20=(Token)match(input,ID,FOLLOW_ID_in_setter1313); 
			match(input,L_BR,FOLLOW_L_BR_in_setter1315); 
			pushFollow(FOLLOW_expression_in_setter1319);
			f=expression();
			state._fsp--;

			match(input,R_BR,FOLLOW_R_BR_in_setter1321); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:243:29: ( L_BR s= expression R_BR )?
			int alt14=2;
			int LA14_0 = input.LA(1);
			if ( (LA14_0==L_BR) ) {
				alt14=1;
			}
			switch (alt14) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:243:30: L_BR s= expression R_BR
					{
					match(input,L_BR,FOLLOW_L_BR_in_setter1324); 
					pushFollow(FOLLOW_expression_in_setter1328);
					s=expression();
					state._fsp--;

					match(input,R_BR,FOLLOW_R_BR_in_setter1330); 
					}
					break;

			}

			match(input,ASSIGN,FOLLOW_ASSIGN_in_setter1334); 
			pushFollow(FOLLOW_expression_in_setter1338);
			val=expression();
			state._fsp--;


			   if(!blocks.containsVarInAllBlocks((ID20!=null?ID20.getText():null))) {
			      errors.addError("Undefined variable " + (ID20!=null?ID20.getText():null) + ".",(ID20!=null?ID20.getLine():0));
			   } else if ((s!=null?((StringTemplate)s.getTemplate()):null) != null) {
			      retval.st = templateLib.getInstanceOf("matrixSetter",new STAttrMap().put("var", (ID20!=null?ID20.getText():null)).put("frst", (f!=null?((StringTemplate)f.getTemplate()):null)).put("second", (s!=null?((StringTemplate)s.getTemplate()):null)).put("value", (val!=null?((StringTemplate)val.getTemplate()):null)));
			   } else {
			      retval.st = templateLib.getInstanceOf("vectorSetter",new STAttrMap().put("var", (ID20!=null?ID20.getText():null)).put("frst", (f!=null?((StringTemplate)f.getTemplate()):null)).put("value", (val!=null?((StringTemplate)val.getTemplate()):null)));
			   }
			  
			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "setter"


	public static class ifElseStat_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "ifElseStat"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:255:1: ifElseStat : IF L_RBR log_expr R_RBR COLON (stats+= statement )* ( ELSE (else_stats+= statement )* )? END -> ifElseStat(cond=$log_expr.ststats=$statselse_stats=$else_stats);
	public final MatrixParser.ifElseStat_return ifElseStat() throws RecognitionException {
		MatrixParser.ifElseStat_return retval = new MatrixParser.ifElseStat_return();
		retval.start = input.LT(1);

		List<Object> list_stats=null;
		List<Object> list_else_stats=null;
		ParserRuleReturnScope log_expr21 =null;
		RuleReturnScope stats = null;
		RuleReturnScope else_stats = null;
		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:255:11: ( IF L_RBR log_expr R_RBR COLON (stats+= statement )* ( ELSE (else_stats+= statement )* )? END -> ifElseStat(cond=$log_expr.ststats=$statselse_stats=$else_stats))
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:256:3: IF L_RBR log_expr R_RBR COLON (stats+= statement )* ( ELSE (else_stats+= statement )* )? END
			{
			match(input,IF,FOLLOW_IF_in_ifElseStat1354); 
			match(input,L_RBR,FOLLOW_L_RBR_in_ifElseStat1356); 
			pushFollow(FOLLOW_log_expr_in_ifElseStat1358);
			log_expr21=log_expr();
			state._fsp--;

			match(input,R_RBR,FOLLOW_R_RBR_in_ifElseStat1360); 
			match(input,COLON,FOLLOW_COLON_in_ifElseStat1362); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:258:7: (stats+= statement )*
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( (LA15_0==FOR||(LA15_0 >= ID && LA15_0 <= IF)||LA15_0==PUTS||LA15_0==REPEAT||(LA15_0 >= VAR && LA15_0 <= WHILE)) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:258:8: stats+= statement
					{
					pushFollow(FOLLOW_statement_in_ifElseStat1380);
					stats=statement();
					state._fsp--;

					if (list_stats==null) list_stats=new ArrayList<Object>();
					list_stats.add(stats.getTemplate());
					}
					break;

				default :
					break loop15;
				}
			}

			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:259:3: ( ELSE (else_stats+= statement )* )?
			int alt17=2;
			int LA17_0 = input.LA(1);
			if ( (LA17_0==ELSE) ) {
				alt17=1;
			}
			switch (alt17) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:259:4: ELSE (else_stats+= statement )*
					{
					match(input,ELSE,FOLLOW_ELSE_in_ifElseStat1387); 
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:260:7: (else_stats+= statement )*
					loop16:
					while (true) {
						int alt16=2;
						int LA16_0 = input.LA(1);
						if ( (LA16_0==FOR||(LA16_0 >= ID && LA16_0 <= IF)||LA16_0==PUTS||LA16_0==REPEAT||(LA16_0 >= VAR && LA16_0 <= WHILE)) ) {
							alt16=1;
						}

						switch (alt16) {
						case 1 :
							// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:260:8: else_stats+= statement
							{
							pushFollow(FOLLOW_statement_in_ifElseStat1398);
							else_stats=statement();
							state._fsp--;

							if (list_else_stats==null) list_else_stats=new ArrayList<Object>();
							list_else_stats.add(else_stats.getTemplate());
							}
							break;

						default :
							break loop16;
						}
					}

					}
					break;

			}

			match(input,END,FOLLOW_END_in_ifElseStat1406); 
			// TEMPLATE REWRITE
			// 262:3: -> ifElseStat(cond=$log_expr.ststats=$statselse_stats=$else_stats)
			{
				retval.st = templateLib.getInstanceOf("ifElseStat",new STAttrMap().put("cond", (log_expr21!=null?((StringTemplate)log_expr21.getTemplate()):null)).put("stats", list_stats).put("else_stats", list_else_stats));
			}



			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "ifElseStat"


	public static class forStat_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "forStat"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:265:1: forStat : FOR L_RBR (a= var_define )? SEMI ( log_expr )? SEMI (b= var_define )? R_RBR COLON (stats+= statement )* END -> forStat(init=$a.stcond=$log_expr.stassign=$b.ststats=$stats);
	public final MatrixParser.forStat_return forStat() throws RecognitionException {
		MatrixParser.forStat_return retval = new MatrixParser.forStat_return();
		retval.start = input.LT(1);

		List<Object> list_stats=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope log_expr22 =null;
		RuleReturnScope stats = null;
		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:265:8: ( FOR L_RBR (a= var_define )? SEMI ( log_expr )? SEMI (b= var_define )? R_RBR COLON (stats+= statement )* END -> forStat(init=$a.stcond=$log_expr.stassign=$b.ststats=$stats))
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:266:3: FOR L_RBR (a= var_define )? SEMI ( log_expr )? SEMI (b= var_define )? R_RBR COLON (stats+= statement )* END
			{
			match(input,FOR,FOLLOW_FOR_in_forStat1437); 
			match(input,L_RBR,FOLLOW_L_RBR_in_forStat1439); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:266:13: (a= var_define )?
			int alt18=2;
			int LA18_0 = input.LA(1);
			if ( (LA18_0==ID||LA18_0==VAR) ) {
				alt18=1;
			}
			switch (alt18) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:266:14: a= var_define
					{
					pushFollow(FOLLOW_var_define_in_forStat1444);
					a=var_define();
					state._fsp--;

					}
					break;

			}

			match(input,SEMI,FOLLOW_SEMI_in_forStat1448); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:266:34: ( log_expr )?
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( (LA19_0==ID||(LA19_0 >= L_BR && LA19_0 <= L_RBR)||LA19_0==MINUS||LA19_0==NUMBER||LA19_0==UNDEFINED) ) {
				alt19=1;
			}
			switch (alt19) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:266:35: log_expr
					{
					pushFollow(FOLLOW_log_expr_in_forStat1451);
					log_expr22=log_expr();
					state._fsp--;

					}
					break;

			}

			match(input,SEMI,FOLLOW_SEMI_in_forStat1455); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:266:51: (b= var_define )?
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( (LA20_0==ID||LA20_0==VAR) ) {
				alt20=1;
			}
			switch (alt20) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:266:52: b= var_define
					{
					pushFollow(FOLLOW_var_define_in_forStat1460);
					b=var_define();
					state._fsp--;

					}
					break;

			}

			match(input,R_RBR,FOLLOW_R_RBR_in_forStat1464); 
			match(input,COLON,FOLLOW_COLON_in_forStat1466); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:268:7: (stats+= statement )*
			loop21:
			while (true) {
				int alt21=2;
				int LA21_0 = input.LA(1);
				if ( (LA21_0==FOR||(LA21_0 >= ID && LA21_0 <= IF)||LA21_0==PUTS||LA21_0==REPEAT||(LA21_0 >= VAR && LA21_0 <= WHILE)) ) {
					alt21=1;
				}

				switch (alt21) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:268:8: stats+= statement
					{
					pushFollow(FOLLOW_statement_in_forStat1484);
					stats=statement();
					state._fsp--;

					if (list_stats==null) list_stats=new ArrayList<Object>();
					list_stats.add(stats.getTemplate());
					}
					break;

				default :
					break loop21;
				}
			}

			match(input,END,FOLLOW_END_in_forStat1491); 
			// TEMPLATE REWRITE
			// 270:3: -> forStat(init=$a.stcond=$log_expr.stassign=$b.ststats=$stats)
			{
				retval.st = templateLib.getInstanceOf("forStat",new STAttrMap().put("init", (a!=null?((StringTemplate)a.getTemplate()):null)).put("cond", (log_expr22!=null?((StringTemplate)log_expr22.getTemplate()):null)).put("assign", (b!=null?((StringTemplate)b.getTemplate()):null)).put("stats", list_stats));
			}



			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "forStat"


	public static class whileStat_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "whileStat"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:275:1: whileStat : WHILE L_RBR log_expr R_RBR COLON (stats+= statement )* END -> whileStat(cond=$log_expr.ststats=$stats);
	public final MatrixParser.whileStat_return whileStat() throws RecognitionException {
		MatrixParser.whileStat_return retval = new MatrixParser.whileStat_return();
		retval.start = input.LT(1);

		List<Object> list_stats=null;
		ParserRuleReturnScope log_expr23 =null;
		RuleReturnScope stats = null;
		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:275:10: ( WHILE L_RBR log_expr R_RBR COLON (stats+= statement )* END -> whileStat(cond=$log_expr.ststats=$stats))
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:276:3: WHILE L_RBR log_expr R_RBR COLON (stats+= statement )* END
			{
			match(input,WHILE,FOLLOW_WHILE_in_whileStat1555); 
			match(input,L_RBR,FOLLOW_L_RBR_in_whileStat1557); 
			pushFollow(FOLLOW_log_expr_in_whileStat1559);
			log_expr23=log_expr();
			state._fsp--;

			match(input,R_RBR,FOLLOW_R_RBR_in_whileStat1561); 
			match(input,COLON,FOLLOW_COLON_in_whileStat1563); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:278:7: (stats+= statement )*
			loop22:
			while (true) {
				int alt22=2;
				int LA22_0 = input.LA(1);
				if ( (LA22_0==FOR||(LA22_0 >= ID && LA22_0 <= IF)||LA22_0==PUTS||LA22_0==REPEAT||(LA22_0 >= VAR && LA22_0 <= WHILE)) ) {
					alt22=1;
				}

				switch (alt22) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:278:8: stats+= statement
					{
					pushFollow(FOLLOW_statement_in_whileStat1581);
					stats=statement();
					state._fsp--;

					if (list_stats==null) list_stats=new ArrayList<Object>();
					list_stats.add(stats.getTemplate());
					}
					break;

				default :
					break loop22;
				}
			}

			match(input,END,FOLLOW_END_in_whileStat1587); 
			// TEMPLATE REWRITE
			// 280:3: -> whileStat(cond=$log_expr.ststats=$stats)
			{
				retval.st = templateLib.getInstanceOf("whileStat",new STAttrMap().put("cond", (log_expr23!=null?((StringTemplate)log_expr23.getTemplate()):null)).put("stats", list_stats));
			}



			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "whileStat"


	public static class untilStat_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "untilStat"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:283:1: untilStat : REPEAT COLON (stats+= statement )* UNTIL L_RBR log_expr R_RBR -> untilStat(cond=$log_expr.ststats=$stats);
	public final MatrixParser.untilStat_return untilStat() throws RecognitionException {
		MatrixParser.untilStat_return retval = new MatrixParser.untilStat_return();
		retval.start = input.LT(1);

		List<Object> list_stats=null;
		ParserRuleReturnScope log_expr24 =null;
		RuleReturnScope stats = null;
		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:283:10: ( REPEAT COLON (stats+= statement )* UNTIL L_RBR log_expr R_RBR -> untilStat(cond=$log_expr.ststats=$stats))
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:284:3: REPEAT COLON (stats+= statement )* UNTIL L_RBR log_expr R_RBR
			{
			match(input,REPEAT,FOLLOW_REPEAT_in_untilStat1614); 
			match(input,COLON,FOLLOW_COLON_in_untilStat1616); 
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:286:7: (stats+= statement )*
			loop23:
			while (true) {
				int alt23=2;
				int LA23_0 = input.LA(1);
				if ( (LA23_0==FOR||(LA23_0 >= ID && LA23_0 <= IF)||LA23_0==PUTS||LA23_0==REPEAT||(LA23_0 >= VAR && LA23_0 <= WHILE)) ) {
					alt23=1;
				}

				switch (alt23) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:286:8: stats+= statement
					{
					pushFollow(FOLLOW_statement_in_untilStat1634);
					stats=statement();
					state._fsp--;

					if (list_stats==null) list_stats=new ArrayList<Object>();
					list_stats.add(stats.getTemplate());
					}
					break;

				default :
					break loop23;
				}
			}

			match(input,UNTIL,FOLLOW_UNTIL_in_untilStat1640); 
			match(input,L_RBR,FOLLOW_L_RBR_in_untilStat1642); 
			pushFollow(FOLLOW_log_expr_in_untilStat1644);
			log_expr24=log_expr();
			state._fsp--;

			match(input,R_RBR,FOLLOW_R_RBR_in_untilStat1646); 
			// TEMPLATE REWRITE
			// 288:3: -> untilStat(cond=$log_expr.ststats=$stats)
			{
				retval.st = templateLib.getInstanceOf("untilStat",new STAttrMap().put("cond", (log_expr24!=null?((StringTemplate)log_expr24.getTemplate()):null)).put("stats", list_stats));
			}



			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "untilStat"


	public static class log_expr_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "log_expr"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:291:1: log_expr : sublog_expr (op= ( DISJ | CONJ ) l= log_expr )? ;
	public final MatrixParser.log_expr_return log_expr() throws RecognitionException {
		MatrixParser.log_expr_return retval = new MatrixParser.log_expr_return();
		retval.start = input.LT(1);

		Token op=null;
		ParserRuleReturnScope l =null;
		ParserRuleReturnScope sublog_expr25 =null;

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:291:9: ( sublog_expr (op= ( DISJ | CONJ ) l= log_expr )? )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:292:3: sublog_expr (op= ( DISJ | CONJ ) l= log_expr )?
			{
			pushFollow(FOLLOW_sublog_expr_in_log_expr1673);
			sublog_expr25=sublog_expr();
			state._fsp--;

			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:292:15: (op= ( DISJ | CONJ ) l= log_expr )?
			int alt24=2;
			int LA24_0 = input.LA(1);
			if ( (LA24_0==CONJ||LA24_0==DISJ) ) {
				alt24=1;
			}
			switch (alt24) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:292:16: op= ( DISJ | CONJ ) l= log_expr
					{
					op=input.LT(1);
					if ( input.LA(1)==CONJ||input.LA(1)==DISJ ) {
						input.consume();
						state.errorRecovery=false;
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					pushFollow(FOLLOW_log_expr_in_log_expr1686);
					l=log_expr();
					state._fsp--;

					}
					break;

			}


			   if(op != null) {
			      if((op!=null?op.getText():null).equals("||")) {
			        retval.st = templateLib.getInstanceOf("disjunction",new STAttrMap().put("frst", (sublog_expr25!=null?((StringTemplate)sublog_expr25.getTemplate()):null)).put("second", (l!=null?((StringTemplate)l.getTemplate()):null)));
			      } else {
			        retval.st = templateLib.getInstanceOf("conjunction",new STAttrMap().put("frst", (sublog_expr25!=null?((StringTemplate)sublog_expr25.getTemplate()):null)).put("second", (l!=null?((StringTemplate)l.getTemplate()):null)));
			      }
			   } else {
			      retval.st = (sublog_expr25!=null?((StringTemplate)sublog_expr25.getTemplate()):null);
			   }
			  
			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "log_expr"


	public static class sublog_expr_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "sublog_expr"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:306:1: sublog_expr : first= expression LOG_SIGN second= expression ;
	public final MatrixParser.sublog_expr_return sublog_expr() throws RecognitionException {
		MatrixParser.sublog_expr_return retval = new MatrixParser.sublog_expr_return();
		retval.start = input.LT(1);

		Token LOG_SIGN26=null;
		ParserRuleReturnScope first =null;
		ParserRuleReturnScope second =null;

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:306:13: (first= expression LOG_SIGN second= expression )
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:307:3: first= expression LOG_SIGN second= expression
			{
			pushFollow(FOLLOW_expression_in_sublog_expr1707);
			first=expression();
			state._fsp--;

			LOG_SIGN26=(Token)match(input,LOG_SIGN,FOLLOW_LOG_SIGN_in_sublog_expr1709); 
			pushFollow(FOLLOW_expression_in_sublog_expr1713);
			second=expression();
			state._fsp--;


			    switch((LOG_SIGN26!=null?LOG_SIGN26.getText():null)) {
			      case "==":
			        retval.st = templateLib.getInstanceOf("equal",new STAttrMap().put("frst", (first!=null?((StringTemplate)first.getTemplate()):null)).put("second", (second!=null?((StringTemplate)second.getTemplate()):null)));
			        break;
			      case "!=":
			        retval.st = templateLib.getInstanceOf("notEqual",new STAttrMap().put("frst", (first!=null?((StringTemplate)first.getTemplate()):null)).put("second", (second!=null?((StringTemplate)second.getTemplate()):null)));
			        break;
			      case ">=":
			        retval.st = templateLib.getInstanceOf("greaterEqual",new STAttrMap().put("frst", (first!=null?((StringTemplate)first.getTemplate()):null)).put("second", (second!=null?((StringTemplate)second.getTemplate()):null)));
			        break;
			      case "<=":
			        retval.st = templateLib.getInstanceOf("lessEqual",new STAttrMap().put("frst", (first!=null?((StringTemplate)first.getTemplate()):null)).put("second", (second!=null?((StringTemplate)second.getTemplate()):null)));
			        break;
			      case ">":
			        retval.st = templateLib.getInstanceOf("greater",new STAttrMap().put("frst", (first!=null?((StringTemplate)first.getTemplate()):null)).put("second", (second!=null?((StringTemplate)second.getTemplate()):null)));
			        break;
			      case "<":
			        retval.st = templateLib.getInstanceOf("less",new STAttrMap().put("frst", (first!=null?((StringTemplate)first.getTemplate()):null)).put("second", (second!=null?((StringTemplate)second.getTemplate()):null)));
			        break;
			    }
			  
			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "sublog_expr"


	public static class var_define_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "var_define"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:332:1: var_define : ( VAR ID -> var_default_define(name=$ID.text)| VAR ID ASSIGN var_value= expression -> var_define(name=$ID.textvalue=$var_value.st)| ID ASSIGN var_value= expression );
	public final MatrixParser.var_define_return var_define() throws RecognitionException {
		MatrixParser.var_define_return retval = new MatrixParser.var_define_return();
		retval.start = input.LT(1);

		Token ID27=null;
		Token ID28=null;
		Token ID29=null;
		ParserRuleReturnScope var_value =null;

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:332:11: ( VAR ID -> var_default_define(name=$ID.text)| VAR ID ASSIGN var_value= expression -> var_define(name=$ID.textvalue=$var_value.st)| ID ASSIGN var_value= expression )
			int alt25=3;
			int LA25_0 = input.LA(1);
			if ( (LA25_0==VAR) ) {
				int LA25_1 = input.LA(2);
				if ( (LA25_1==ID) ) {
					int LA25_3 = input.LA(3);
					if ( (LA25_3==ASSIGN) ) {
						alt25=2;
					}
					else if ( ((LA25_3 >= ELSE && LA25_3 <= FOR)||(LA25_3 >= ID && LA25_3 <= IF)||LA25_3==PUTS||(LA25_3 >= REPEAT && LA25_3 <= RETURN)||(LA25_3 >= R_RBR && LA25_3 <= SEMI)||(LA25_3 >= UNTIL && LA25_3 <= WHILE)) ) {
						alt25=1;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 25, 3, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 25, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA25_0==ID) ) {
				alt25=3;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 25, 0, input);
				throw nvae;
			}

			switch (alt25) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:333:3: VAR ID
					{
					match(input,VAR,FOLLOW_VAR_in_var_define1731); 
					ID27=(Token)match(input,ID,FOLLOW_ID_in_var_define1733); 

					   if(blocks.containsVar((ID27!=null?ID27.getText():null))) {
					      errors.addError("Duplicate local variable " + (ID27!=null?ID27.getText():null), (ID27!=null?ID27.getLine():0));
					   } else {
					      blocks.addVar((ID27!=null?ID27.getText():null), (ID27!=null?ID27.getLine():0));
					   }
					  
					// TEMPLATE REWRITE
					// 341:3: -> var_default_define(name=$ID.text)
					{
						retval.st = templateLib.getInstanceOf("var_default_define",new STAttrMap().put("name", (ID27!=null?ID27.getText():null)));
					}



					}
					break;
				case 2 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:343:3: VAR ID ASSIGN var_value= expression
					{
					match(input,VAR,FOLLOW_VAR_in_var_define1758); 
					ID28=(Token)match(input,ID,FOLLOW_ID_in_var_define1760); 
					match(input,ASSIGN,FOLLOW_ASSIGN_in_var_define1762); 
					pushFollow(FOLLOW_expression_in_var_define1766);
					var_value=expression();
					state._fsp--;


					   if(blocks.containsVar((ID28!=null?ID28.getText():null))) {
					      errors.addError("Duplicate local variable " + (ID28!=null?ID28.getText():null) + ".", (ID28!=null?ID28.getLine():0));
					   } else {
					      blocks.addVar((ID28!=null?ID28.getText():null), (ID28!=null?ID28.getLine():0));
					   } 
					  
					// TEMPLATE REWRITE
					// 351:3: -> var_define(name=$ID.textvalue=$var_value.st)
					{
						retval.st = templateLib.getInstanceOf("var_define",new STAttrMap().put("name", (ID28!=null?ID28.getText():null)).put("value", (var_value!=null?((StringTemplate)var_value.getTemplate()):null)));
					}



					}
					break;
				case 3 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:353:3: ID ASSIGN var_value= expression
					{
					ID29=(Token)match(input,ID,FOLLOW_ID_in_var_define1798); 
					match(input,ASSIGN,FOLLOW_ASSIGN_in_var_define1800); 
					pushFollow(FOLLOW_expression_in_var_define1804);
					var_value=expression();
					state._fsp--;


					   if(blocks.containsVar((ID29!=null?ID29.getText():null))) {
					      retval.st = templateLib.getInstanceOf("var_assign",new STAttrMap().put("name", (ID29!=null?ID29.getText():null)).put("value", (var_value!=null?((StringTemplate)var_value.getTemplate()):null)));
					   } else {
								blocks.addVar((ID29!=null?ID29.getText():null), (ID29!=null?ID29.getLine():0));
					      retval.st = templateLib.getInstanceOf("var_define",new STAttrMap().put("name", (ID29!=null?ID29.getText():null)).put("value", (var_value!=null?((StringTemplate)var_value.getTemplate()):null)));
					   }
					  
					}
					break;

			}
			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "var_define"


	public static class number_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "number"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:364:1: number : ( MINUS )? NUMBER -> {new StringTemplate(($MINUS != null ? $MINUS.text : \"\") + $NUMBER.text);};
	public final MatrixParser.number_return number() throws RecognitionException {
		MatrixParser.number_return retval = new MatrixParser.number_return();
		retval.start = input.LT(1);

		Token MINUS30=null;
		Token NUMBER31=null;

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:364:7: ( ( MINUS )? NUMBER -> {new StringTemplate(($MINUS != null ? $MINUS.text : \"\") + $NUMBER.text);})
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:365:3: ( MINUS )? NUMBER
			{
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:365:3: ( MINUS )?
			int alt26=2;
			int LA26_0 = input.LA(1);
			if ( (LA26_0==MINUS) ) {
				alt26=1;
			}
			switch (alt26) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:365:3: MINUS
					{
					MINUS30=(Token)match(input,MINUS,FOLLOW_MINUS_in_number1820); 
					}
					break;

			}

			NUMBER31=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_number1823); 
			// TEMPLATE REWRITE
			// 366:3: -> {new StringTemplate(($MINUS != null ? $MINUS.text : \"\") + $NUMBER.text);}
			{
				retval.st = new StringTemplate((MINUS30 != null ? (MINUS30!=null?MINUS30.getText():null) : "") + (NUMBER31!=null?NUMBER31.getText():null));;
			}



			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "number"


	public static class initializationVector_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "initializationVector"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:369:1: initializationVector : L_BR vals+= number ( COMMA vals+= number )* R_BR -> initVector(vals=$vals);
	public final MatrixParser.initializationVector_return initializationVector() throws RecognitionException {
		MatrixParser.initializationVector_return retval = new MatrixParser.initializationVector_return();
		retval.start = input.LT(1);

		List<Object> list_vals=null;
		RuleReturnScope vals = null;
		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:369:21: ( L_BR vals+= number ( COMMA vals+= number )* R_BR -> initVector(vals=$vals))
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:370:3: L_BR vals+= number ( COMMA vals+= number )* R_BR
			{
			match(input,L_BR,FOLLOW_L_BR_in_initializationVector1841); 
			pushFollow(FOLLOW_number_in_initializationVector1847);
			vals=number();
			state._fsp--;

			if (list_vals==null) list_vals=new ArrayList<Object>();
			list_vals.add(vals.getTemplate());
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:370:22: ( COMMA vals+= number )*
			loop27:
			while (true) {
				int alt27=2;
				int LA27_0 = input.LA(1);
				if ( (LA27_0==COMMA) ) {
					alt27=1;
				}

				switch (alt27) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:370:23: COMMA vals+= number
					{
					match(input,COMMA,FOLLOW_COMMA_in_initializationVector1849); 
					pushFollow(FOLLOW_number_in_initializationVector1855);
					vals=number();
					state._fsp--;

					if (list_vals==null) list_vals=new ArrayList<Object>();
					list_vals.add(vals.getTemplate());
					}
					break;

				default :
					break loop27;
				}
			}

			match(input,R_BR,FOLLOW_R_BR_in_initializationVector1859); 
			// TEMPLATE REWRITE
			// 371:3: -> initVector(vals=$vals)
			{
				retval.st = templateLib.getInstanceOf("initVector",new STAttrMap().put("vals", list_vals));
			}



			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "initializationVector"


	protected static class initializationMatrix_scope {
		int matrixRow;
		int matrixColumn;
	}
	protected Stack<initializationMatrix_scope> initializationMatrix_stack = new Stack<initializationMatrix_scope>();

	public static class initializationMatrix_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "initializationMatrix"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:374:1: initializationMatrix : L_CBR vals+= number ( ( COMMA vals+= number ) | BOR vals+= number )* R_CBR -> initMatrix(vals=$valsrows=$initializationMatrix::matrixRow);
	public final MatrixParser.initializationMatrix_return initializationMatrix() throws RecognitionException {
		initializationMatrix_stack.push(new initializationMatrix_scope());
		MatrixParser.initializationMatrix_return retval = new MatrixParser.initializationMatrix_return();
		retval.start = input.LT(1);

		Token L_CBR32=null;
		List<Object> list_vals=null;
		RuleReturnScope vals = null;

		  initializationMatrix_stack.peek().matrixRow = 1;

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:381:2: ( L_CBR vals+= number ( ( COMMA vals+= number ) | BOR vals+= number )* R_CBR -> initMatrix(vals=$valsrows=$initializationMatrix::matrixRow))
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:382:3: L_CBR vals+= number ( ( COMMA vals+= number ) | BOR vals+= number )* R_CBR
			{
			L_CBR32=(Token)match(input,L_CBR,FOLLOW_L_CBR_in_initializationMatrix1893); 
			pushFollow(FOLLOW_number_in_initializationMatrix1899);
			vals=number();
			state._fsp--;

			if (list_vals==null) list_vals=new ArrayList<Object>();
			list_vals.add(vals.getTemplate());
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:383:5: ( ( COMMA vals+= number ) | BOR vals+= number )*
			loop28:
			while (true) {
				int alt28=3;
				int LA28_0 = input.LA(1);
				if ( (LA28_0==COMMA) ) {
					alt28=1;
				}
				else if ( (LA28_0==BOR) ) {
					alt28=2;
				}

				switch (alt28) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:383:6: ( COMMA vals+= number )
					{
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:383:6: ( COMMA vals+= number )
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:383:7: COMMA vals+= number
					{
					match(input,COMMA,FOLLOW_COMMA_in_initializationMatrix1907); 
					pushFollow(FOLLOW_number_in_initializationMatrix1913);
					vals=number();
					state._fsp--;

					if (list_vals==null) list_vals=new ArrayList<Object>();
					list_vals.add(vals.getTemplate());
					}

					}
					break;
				case 2 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:384:9: BOR vals+= number
					{
					match(input,BOR,FOLLOW_BOR_in_initializationMatrix1924); 
					initializationMatrix_stack.peek().matrixRow++;
					pushFollow(FOLLOW_number_in_initializationMatrix1931);
					vals=number();
					state._fsp--;

					if (list_vals==null) list_vals=new ArrayList<Object>();
					list_vals.add(vals.getTemplate());
					}
					break;

				default :
					break loop28;
				}
			}

			match(input,R_CBR,FOLLOW_R_CBR_in_initializationMatrix1934); 

			   initializationMatrix_stack.peek().matrixColumn = list_vals.size() / initializationMatrix_stack.peek().matrixRow;
			   if(list_vals.size() != initializationMatrix_stack.peek().matrixRow * initializationMatrix_stack.peek().matrixColumn){
			       errors.addError("Initialization matrix error", (L_CBR32!=null?L_CBR32.getLine():0));
			   }
			  
			// TEMPLATE REWRITE
			// 391:3: -> initMatrix(vals=$valsrows=$initializationMatrix::matrixRow)
			{
				retval.st = templateLib.getInstanceOf("initMatrix",new STAttrMap().put("vals", list_vals).put("rows", initializationMatrix_stack.peek().matrixRow));
			}



			}

			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
			initializationMatrix_stack.pop();
		}
		return retval;
	}
	// $ANTLR end "initializationMatrix"


	public static class putStat_return extends ParserRuleReturnScope {
		public StringTemplate st;
		public Object getTemplate() { return st; }
		public String toString() { return st==null?null:st.toString(); }
	};


	// $ANTLR start "putStat"
	// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:394:1: putStat : ( PUTS STRING -> printString(s=$STRING.text)| PUTS expression -> printVar(var=$expression.st));
	public final MatrixParser.putStat_return putStat() throws RecognitionException {
		MatrixParser.putStat_return retval = new MatrixParser.putStat_return();
		retval.start = input.LT(1);

		Token STRING33=null;
		ParserRuleReturnScope expression34 =null;

		try {
			// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:394:8: ( PUTS STRING -> printString(s=$STRING.text)| PUTS expression -> printVar(var=$expression.st))
			int alt29=2;
			int LA29_0 = input.LA(1);
			if ( (LA29_0==PUTS) ) {
				int LA29_1 = input.LA(2);
				if ( (LA29_1==STRING) ) {
					alt29=1;
				}
				else if ( (LA29_1==ID||(LA29_1 >= L_BR && LA29_1 <= L_RBR)||LA29_1==MINUS||LA29_1==NUMBER||LA29_1==UNDEFINED) ) {
					alt29=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 29, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 29, 0, input);
				throw nvae;
			}

			switch (alt29) {
				case 1 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:395:3: PUTS STRING
					{
					match(input,PUTS,FOLLOW_PUTS_in_putStat1970); 
					STRING33=(Token)match(input,STRING,FOLLOW_STRING_in_putStat1972); 
					// TEMPLATE REWRITE
					// 396:3: -> printString(s=$STRING.text)
					{
						retval.st = templateLib.getInstanceOf("printString",new STAttrMap().put("s", (STRING33!=null?STRING33.getText():null)));
					}



					}
					break;
				case 2 :
					// G:\\wsi2\\MatrixCompiler\\src\\Matrix.g:398:3: PUTS expression
					{
					match(input,PUTS,FOLLOW_PUTS_in_putStat1993); 
					pushFollow(FOLLOW_expression_in_putStat1995);
					expression34=expression();
					state._fsp--;

					// TEMPLATE REWRITE
					// 399:3: -> printVar(var=$expression.st)
					{
						retval.st = templateLib.getInstanceOf("printVar",new STAttrMap().put("var", (expression34!=null?((StringTemplate)expression34.getTemplate()):null)));
					}



					}
					break;

			}
			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "putStat"

	// Delegated rules



	public static final BitSet FOLLOW_functionDeclaration_in_program679 = new BitSet(new long[]{0x0000000002001000L});
	public static final BitSet FOLLOW_MAIN_in_program685 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_program687 = new BitSet(new long[]{0x0000C00A001B0000L});
	public static final BitSet FOLLOW_statement_in_program704 = new BitSet(new long[]{0x0000C00A001B0000L});
	public static final BitSet FOLLOW_END_in_program714 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DEFINE_in_functionDeclaration759 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_ID_in_functionDeclaration763 = new BitSet(new long[]{0x0000000001000080L});
	public static final BitSet FOLLOW_L_RBR_in_functionDeclaration766 = new BitSet(new long[]{0x0000010000080000L});
	public static final BitSet FOLLOW_ID_in_functionDeclaration771 = new BitSet(new long[]{0x0000010000000200L});
	public static final BitSet FOLLOW_COMMA_in_functionDeclaration793 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_ID_in_functionDeclaration797 = new BitSet(new long[]{0x0000010000000200L});
	public static final BitSet FOLLOW_R_RBR_in_functionDeclaration804 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_functionDeclaration808 = new BitSet(new long[]{0x0000C01A001B0000L});
	public static final BitSet FOLLOW_statement_in_functionDeclaration823 = new BitSet(new long[]{0x0000C01A001B0000L});
	public static final BitSet FOLLOW_RETURN_in_functionDeclaration830 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_functionDeclaration834 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_END_in_functionDeclaration844 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_functionCall858 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_L_RBR_in_functionCall860 = new BitSet(new long[]{0x0000110025C80000L});
	public static final BitSet FOLLOW_expression_in_functionCall865 = new BitSet(new long[]{0x0000010000000200L});
	public static final BitSet FOLLOW_COMMA_in_functionCall878 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_functionCall882 = new BitSet(new long[]{0x0000010000000200L});
	public static final BitSet FOLLOW_R_RBR_in_functionCall888 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subexpr_in_expression926 = new BitSet(new long[]{0x000000008C004002L});
	public static final BitSet FOLLOW_set_in_expression931 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_subexpr_in_expression943 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_functionCall_in_subexpr961 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_L_RBR_in_subexpr975 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_subexpr977 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_R_RBR_in_subexpr979 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_subexpr993 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_number_in_subexpr1011 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_initializationVector_in_subexpr1032 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_initializationMatrix_in_subexpr1046 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_getter_in_subexpr1060 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sizeRowColumn_in_subexpr1074 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_UNDEFINED_in_subexpr1088 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_sizeRowColumn1108 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_POINT_in_sizeRowColumn1110 = new BitSet(new long[]{0x0000042000000100L});
	public static final BitSet FOLLOW_set_in_sizeRowColumn1114 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_getter1136 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_L_BR_in_getter1138 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_getter1142 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_R_BR_in_getter1144 = new BitSet(new long[]{0x0000000000400002L});
	public static final BitSet FOLLOW_L_BR_in_getter1147 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_getter1151 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_R_BR_in_getter1153 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_var_define_in_statement1173 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_putStat_in_statement1192 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_functionCall_in_statement1214 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_whileStat_in_statement1231 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_untilStat_in_statement1244 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_forStat_in_statement1257 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ifElseStat_in_statement1272 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_setter_in_statement1284 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_setter1313 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_L_BR_in_setter1315 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_setter1319 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_R_BR_in_setter1321 = new BitSet(new long[]{0x0000000000400020L});
	public static final BitSet FOLLOW_L_BR_in_setter1324 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_setter1328 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_R_BR_in_setter1330 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ASSIGN_in_setter1334 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_setter1338 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_ifElseStat1354 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_L_RBR_in_ifElseStat1356 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_log_expr_in_ifElseStat1358 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_R_RBR_in_ifElseStat1360 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_ifElseStat1362 = new BitSet(new long[]{0x0000C00A001B8000L});
	public static final BitSet FOLLOW_statement_in_ifElseStat1380 = new BitSet(new long[]{0x0000C00A001B8000L});
	public static final BitSet FOLLOW_ELSE_in_ifElseStat1387 = new BitSet(new long[]{0x0000C00A001B0000L});
	public static final BitSet FOLLOW_statement_in_ifElseStat1398 = new BitSet(new long[]{0x0000C00A001B0000L});
	public static final BitSet FOLLOW_END_in_ifElseStat1406 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FOR_in_forStat1437 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_L_RBR_in_forStat1439 = new BitSet(new long[]{0x0000420000080000L});
	public static final BitSet FOLLOW_var_define_in_forStat1444 = new BitSet(new long[]{0x0000020000000000L});
	public static final BitSet FOLLOW_SEMI_in_forStat1448 = new BitSet(new long[]{0x0000120025C80000L});
	public static final BitSet FOLLOW_log_expr_in_forStat1451 = new BitSet(new long[]{0x0000020000000000L});
	public static final BitSet FOLLOW_SEMI_in_forStat1455 = new BitSet(new long[]{0x0000410000080000L});
	public static final BitSet FOLLOW_var_define_in_forStat1460 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_R_RBR_in_forStat1464 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_forStat1466 = new BitSet(new long[]{0x0000C00A001B0000L});
	public static final BitSet FOLLOW_statement_in_forStat1484 = new BitSet(new long[]{0x0000C00A001B0000L});
	public static final BitSet FOLLOW_END_in_forStat1491 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHILE_in_whileStat1555 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_L_RBR_in_whileStat1557 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_log_expr_in_whileStat1559 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_R_RBR_in_whileStat1561 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_whileStat1563 = new BitSet(new long[]{0x0000C00A001B0000L});
	public static final BitSet FOLLOW_statement_in_whileStat1581 = new BitSet(new long[]{0x0000C00A001B0000L});
	public static final BitSet FOLLOW_END_in_whileStat1587 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_REPEAT_in_untilStat1614 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_untilStat1616 = new BitSet(new long[]{0x0000E00A001A0000L});
	public static final BitSet FOLLOW_statement_in_untilStat1634 = new BitSet(new long[]{0x0000E00A001A0000L});
	public static final BitSet FOLLOW_UNTIL_in_untilStat1640 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_L_RBR_in_untilStat1642 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_log_expr_in_untilStat1644 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_R_RBR_in_untilStat1646 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_sublog_expr_in_log_expr1673 = new BitSet(new long[]{0x0000000000002802L});
	public static final BitSet FOLLOW_set_in_log_expr1678 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_log_expr_in_log_expr1686 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_sublog_expr1707 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_LOG_SIGN_in_sublog_expr1709 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_sublog_expr1713 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_var_define1731 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_ID_in_var_define1733 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_var_define1758 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_ID_in_var_define1760 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ASSIGN_in_var_define1762 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_var_define1766 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_var_define1798 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ASSIGN_in_var_define1800 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_var_define1804 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_MINUS_in_number1820 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_NUMBER_in_number1823 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_L_BR_in_initializationVector1841 = new BitSet(new long[]{0x0000000024000000L});
	public static final BitSet FOLLOW_number_in_initializationVector1847 = new BitSet(new long[]{0x0000004000000200L});
	public static final BitSet FOLLOW_COMMA_in_initializationVector1849 = new BitSet(new long[]{0x0000000024000000L});
	public static final BitSet FOLLOW_number_in_initializationVector1855 = new BitSet(new long[]{0x0000004000000200L});
	public static final BitSet FOLLOW_R_BR_in_initializationVector1859 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_L_CBR_in_initializationMatrix1893 = new BitSet(new long[]{0x0000000024000000L});
	public static final BitSet FOLLOW_number_in_initializationMatrix1899 = new BitSet(new long[]{0x0000008000000240L});
	public static final BitSet FOLLOW_COMMA_in_initializationMatrix1907 = new BitSet(new long[]{0x0000000024000000L});
	public static final BitSet FOLLOW_number_in_initializationMatrix1913 = new BitSet(new long[]{0x0000008000000240L});
	public static final BitSet FOLLOW_BOR_in_initializationMatrix1924 = new BitSet(new long[]{0x0000000024000000L});
	public static final BitSet FOLLOW_number_in_initializationMatrix1931 = new BitSet(new long[]{0x0000008000000240L});
	public static final BitSet FOLLOW_R_CBR_in_initializationMatrix1934 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PUTS_in_putStat1970 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_STRING_in_putStat1972 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PUTS_in_putStat1993 = new BitSet(new long[]{0x0000100025C80000L});
	public static final BitSet FOLLOW_expression_in_putStat1995 = new BitSet(new long[]{0x0000000000000002L});
}
