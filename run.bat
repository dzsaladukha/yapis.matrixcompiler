@echo off
if exist .\src\main\Main.java ( 
"C:\Program Files\Java\jdk1.7.0_79\bin\javac.exe" -d bin -sourcepath src -cp lib\antlr-3.5.2-complete.jar;lib\Jama-1.0.3.jar src\main\Main.java
"C:\Program Files\Java\jdk1.7.0_79\bin\java.exe" -cp bin;lib\antlr-3.5.2-complete.jar;lib\Jama-1.0.3.jar main.Main
) else ( echo System message : source java file doesn't exist. Exiting compilation)